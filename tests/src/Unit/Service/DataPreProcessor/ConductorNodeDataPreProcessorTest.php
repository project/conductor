<?php

namespace Drupal\Tests\conductor\Unit\Service\DataPreProcessor;

use Drupal\conductor\Service\DataPreProcessor\ConductorNodeDataPreProcessor;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Theme\ThemeInitializationInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\Tests\UnitTestCase;

class ConductorNodeDataPreProcessorTest extends UnitTestCase {

  protected ConductorNodeDataPreProcessor $processor;

  public function setUp(): void {
    parent::setUp();
    $this->processor = new ConductorNodeDataPreProcessor(
      $this->createMock(ConfigFactoryInterface::class),
      $this->createMock(EntityTypeManagerInterface::class),
      $this->createMock(RendererInterface::class),
      $this->createMock(ThemeInitializationInterface::class),
      $this->createMock(ThemeManagerInterface::class)
    );
  }

  /**
   * Test removeScriptTags with script tags.
   */
  public function testRemoveScriptTagsRemovesScriptTags(): void {
    $input = '<div>Content</div><script>alert("test")</script>';
    $expected = '<div>Content</div>';
    $result = $this->processor->removeScriptTags($input);

    $this->assertEquals($expected, $result);
  }

  /**
   * Test removeScriptTags with encoded script tags.
   */
  public function testRemoveScriptTagsHandlesEncodedScriptTags(): void {
    $input = '<div>Content</div>&lt;script&gt;alert("test")&lt;/script&gt;';
    $expected = '<div>Content</div>';
    $result = $this->processor->removeScriptTags($input);

    $this->assertEquals($expected, $result);
  }

  /**
   * Test removeScriptTags with empty strings.
   */
  public function testRemoveScriptTagsHandlesEmptyString(): void {
    $input = '';
    $expected = '';
    $result = $this->processor->removeScriptTags($input);

    $this->assertEquals($expected, $result);
  }

}
