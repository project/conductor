<?php

declare(strict_types=1);

namespace Drupal\Tests\conductor\Unit\Service;

use Drupal\conductor\Exception\ConductorApiRequestException;
use Drupal\conductor\Service\Accordion\ConductorAccordionDataMapper;
use Drupal\conductor\Service\APIConnector\ConductorAPIService;
use Drupal\conductor\Service\ConductorManager;
use Drupal\conductor\Service\DataPreProcessor\ConductorNodeDataPreProcessor;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Tests\UnitTestCase;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;

/**
 * @coversDefaultClass \Drupal\conductor\Service\ConductorManager
 * @group conductor
 */
class ConductorManagerTest extends UnitTestCase {

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected MockObject|ConfigFactoryInterface $configFactory;

  /**
   * Conductor Manager service.
   *
   * @var \Drupal\conductor\Service\ConductorManager
   */
  protected ConductorManager $conductorManager;

  /**
   * Conductor Accordion Data Mapper service.
   *
   * @var \Drupal\conductor\Service\Accordion\ConductorAccordionDataMapper|\PHPUnit\Framework\MockObject\MockObject
   */
  protected MockObject|ConductorAccordionDataMapper $conductorAccordionDataMapper;

  /**
   * Conductor API Service.
   *
   * @var \Drupal\conductor\Service\APIConnector\ConductorAPIService|\PHPUnit\Framework\MockObject\MockObject
   */
  protected MockObject|ConductorAPIService $conductorAPIService;

  /**
   * Conductor Node Data Processor service.
   *
   * @var \Drupal\conductor\Service\DataPreProcessor\ConductorNodeDataPreProcessor|\PHPUnit\Framework\MockObject\MockObject
   */
  protected MockObject|ConductorNodeDataPreProcessor $conductorNodeDataProcessor;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected MockObject|LoggerInterface $logger;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->conductorAccordionDataMapper = $this->createMock(ConductorAccordionDataMapper::class);
    $this->conductorAPIService = $this->createMock(ConductorAPIService::class);
    $this->configFactory = $this->createMock(ConfigFactoryInterface::class);
    $this->conductorNodeDataProcessor = $this->createMock(ConductorNodeDataPreProcessor::class);
    $this->logger = $this->createMock(LoggerInterface::class);
    $this->conductorManager = new ConductorManager($this->conductorAccordionDataMapper, $this->conductorAPIService, $this->configFactory, $this->conductorNodeDataProcessor, $this->logger);

    // Mock the TranslationInterface.
    $translation = $this->createMock(TranslationInterface::class);
    $translation->method('translate')
      ->willReturnArgument(0);

    $this->conductorManager->setStringTranslation($translation);
  }

  /**
   * Tests the ConductorManager constructor.
   *
   * @covers ::__construct
   */
  public function testConductorManagerConstructor(): void {
    $this->assertInstanceOf(ConductorManager::class, $this->conductorManager);
  }

  /**
   * Tests the buildAccordion method.
   *
   * @covers ::buildAccordion
   */
  public function testBuildAccordionHandlesValidData(): void {

    $formState = $this->createMock(FormStateInterface::class);
    $response = new AjaxResponse();

    $this->conductorAPIService->method('getAccountId')->willReturn('account_id');
    $this->conductorAPIService->method('getWebPropertyId')
      ->willReturn('web_property_id');
    $this->conductorNodeDataProcessor->method('getBodyCopy')->willReturn('body_copy');
    $this->conductorAPIService->method('getRequestId')->willReturn('request_id');
    $this->conductorAPIService->method('fetchRecommendations')->willReturn(['data']);

    $formState->method('getValue')->willReturn('topic');

    $result = $this->conductorManager->buildAccordion([], $formState, $response);

    $this->assertInstanceOf(AjaxResponse::class, $result);
  }

  /**
   * Tests the buildAccordion method when an exception is thrown.
   */
  public function testBuildAccordionHandlesApiRequestException(): void {

    $formState = $this->createMock(FormStateInterface::class);
    $response = $this->createMock(AjaxResponse::class);

    $response->expects($this->exactly(5))
      ->method('addCommand')
      ->willReturn($response);

    $this->conductorAPIService->method('getAccountId')
      ->willThrowException(new ConductorApiRequestException());

    $result = $this->conductorManager->buildAccordion([], $formState, $response);

    $this->assertInstanceOf(AjaxResponse::class, $result);
  }

  /**
   * Tests the buildAccordion method when an exception is thrown.
   */
  public function testBuildAccordionHandlesGenericException(): void {

    $formState = $this->createMock(FormStateInterface::class);
    $response = $this->createMock(AjaxResponse::class);
    $response->expects($this->exactly(5))
      ->method('addCommand')
      ->willReturn($response);

    $this->conductorAPIService->method('getAccountId')
      ->willThrowException(new \Exception());

    $result = $this->conductorManager->buildAccordion([], $formState, $response);

    $this->assertInstanceOf(AjaxResponse::class, $result);
  }

}
