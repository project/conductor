<?php

declare(strict_types=1);

namespace Drupal\Tests\conductor\Unit\Service;

use Drupal\conductor\Service\APIConnector\ConductorConnector;
use Drupal\conductor\Service\ConductorHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\Entity\ConfigEntityStorage;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Theme\ThemeInitializationInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\node\NodeInterface;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Client;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;

/**
 * @coversDefaultClass \Drupal\conductor\Service\ConductorHelper
 * @group conductor
 */
class ConductorHelperTest extends UnitTestCase {

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected MockObject|ConfigFactoryInterface $configSettings;

  /**
   * Guzzle HTTP client.
   *
   * @var \GuzzleHttp\Client|\PHPUnit\Framework\MockObject\MockObject
   */
  protected Client|MockObject $httpClient;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected LoggerInterface|MockObject $logger;

  /**
   * Conductor Connector service.
   *
   * @var \Drupal\conductor\Service\APIConnector\ConductorConnector|\PHPUnit\Framework\MockObject\MockObject
   */
  protected ConductorConnector|MockObject $connectorService;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected EntityTypeManagerInterface|MockObject $entityTypeManager;

  /**
   * The node type storage handler.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorage|\PHPUnit\Framework\MockObject\MockObject
   */
  protected ConfigEntityStorage|MockObject $nodeTypeStorage;

  /**
   * Entity Storage interface.
   *
   * @var \PHPUnit\Framework\MockObject\MockObject|\Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected MockObject|EntityTypeManagerInterface $entityStorage;

  /**
   * Module Extension List.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList|\PHPUnit\Framework\MockObject\MockObject
   */
  protected ModuleExtensionList|MockObject $moduleExtension;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected RendererInterface|MockObject $renderer;

  /**
   * Theme Initialization Interface.
   *
   * @var \PHPUnit\Framework\MockObject\MockObject|\Drupal\Core\Theme\ThemeInitializationInterface
   */
  protected MockObject|ThemeInitializationInterface $theme;

  /**
   * Theme Manager Interface.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected ThemeManagerInterface|MockObject $themeManager;

  /**
   * Conductor Helper service.
   *
   * @var \Drupal\conductor\Service\ConductorHelper|\PHPUnit\Framework\MockObject\MockObject
   */
  protected ConductorHelper|MockObject $helperService;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->httpClient = $this->createMock(Client::class);
    $this->logger = $this->createMock(LoggerInterface::class);
    $this->configSettings = $this->getConfigFactoryStub([
      'conductor.settings' => [
        'conductor' => [
          'api_key' => '',
          'shared_secret' => '',
          'content_types' => [
            'article',
          ],
        ],
      ],
    ]);
    $this->connectorService = new ConductorConnector($this->configSettings, $this->httpClient, $this->logger);
    $this->entityTypeManager = $this->createMock(EntityTypeManagerInterface::class);
    $this->nodeTypeStorage = $this->createMock(ConfigEntityStorage::class);
    $this->entityStorage = $this->createMock(EntityStorageInterface::class);
    $this->entityStorage->expects($this->any())
      ->method('loadMultiple')
      ->willReturn($this->nodeTypeStorage);
    $this->entityTypeManager->expects($this->any())
      ->method('getStorage')
      ->with('node_type')
      ->willReturn($this->entityStorage);
    $this->moduleExtension = $this->createMock(ModuleExtensionList::class);
    $this->renderer = $this->createMock(RendererInterface::class);
    $this->theme = $this->createMock(ThemeInitializationInterface::class);
    $this->themeManager = $this->createMock(ThemeManagerInterface::class);
    $this->helperService = new ConductorHelper($this->connectorService, $this->entityTypeManager, $this->moduleExtension, $this->renderer, $this->configSettings, $this->logger);
  }

  /**
   * Tests the ConductorHelper constructor.
   *
   * @covers ::__construct
   */
  public function testConductorHelperConstructor(): void {
    $this->assertInstanceOf(ConductorHelper::class, $this->helperService);
  }

  /**
   * Tests check content type validation returns false for non-node form.
   *
   * @covers ::checkContentTypeValidation
   */
  public function testCheckContentTypeValidationReturnsFalseForNonNodeForm(): void {
    $formState = $this->createMock(FormStateInterface::class);

    $result = $this->helperService->checkContentTypeValidation($formState, 'user_form');
    $this->assertFalse($result);
  }

  /**
   * Tests check content type validation returns false for invalid node.
   *
   * @covers ::checkContentTypeValidation
   */
  public function testCheckContentTypeValidationReturnsFalseForInvalidNodeEntity(): void {
    $formState = $this->createMock(FormStateInterface::class);
    $formObject = $this->createMock(EntityFormInterface::class);
    $formObject->method('getEntity')->willReturn($this->createMock(NodeInterface::class));
    $formState->method('getFormObject')->willReturn($formObject);

    $result = $this->helperService->checkContentTypeValidation($formState, 'node_article_form');
    $this->assertFalse($result);
  }

  /**
   * Tests check content type validation fails for non-configured content type.
   *
   * @covers ::checkContentTypeValidation
   */
  public function testCheckContentTypeValidationReturnsFalseForNonConfiguredContentType(): void {
    $formState = $this->createMock(FormStateInterface::class);
    $formObject = $this->createMock(EntityFormInterface::class);
    $node = $this->createMock(NodeInterface::class);
    $node->method('bundle')->willReturn('page');
    $formObject->method('getEntity')->willReturn($node);
    $formState->method('getFormObject')->willReturn($formObject);

    $result = $this->helperService->checkContentTypeValidation($formState, 'node_page_form');
    $this->assertFalse($result);
  }

  /**
   * Tests check content type validation is true for valid node form.
   *
   * @covers ::checkContentTypeValidation
   */
  public function testCheckContentTypeValidationReturnsTrueForValidNodeForm(): void {
    $formState = $this->createMock(FormStateInterface::class);
    $formObject = $this->createMock(EntityFormInterface::class);
    $node = $this->createMock(NodeInterface::class);
    $node->method('bundle')->willReturn('article');
    $formObject->method('getEntity')->willReturn($node);
    $formState->method('getFormObject')->willReturn($formObject);

    $result = $this->helperService->checkContentTypeValidation($formState, 'node_article_form');
    $this->assertTrue($result);
  }

  /**
   * Tests getting content types.
   *
   * @covers ::getContentTypes
   */
  public function testGetContentTypes(): void {
    $content_types = $this->helperService->getContentTypes();
    $this->assertEmpty($content_types);
  }

  /**
   * Tests convert number to thousands.
   *
   * @covers ::convertNumberToK
   */
  public function testConvertNumberToK(): void {
    // Convert Number.
    $converted_number = $this->helperService->convertNumberToK(1000);
    $this->assertEquals('1 K', $converted_number);
  }

  /**
   * Test convert to percent.
   *
   * @covers ::convertToPercent
   */
  public function testConvertToPercent(): void {
    // Convert Number.
    $converted_number = $this->helperService->convertToPercent(0.10);
    $this->assertEquals('10%', $converted_number);
  }

}
