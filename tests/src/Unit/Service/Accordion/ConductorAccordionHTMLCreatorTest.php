<?php

declare(strict_types=1);

namespace Drupal\Tests\conductor\Unit\Service\APIConnector;

use Drupal\conductor\Service\Accordion\ConductorAccordionHTMLCreator;
use Drupal\conductor\Service\ConductorHelper;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Render\Renderer;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Tests\UnitTestCase;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;

/**
 * Tests the ConductorAccordionHTMLCreator class.
 *
 * @coversDefaultClass \Drupal\conductor\Service\Accordion\ConductorAccordionHTMLCreator
 * @group conductor
 */
class ConductorAccordionHTMLCreatorTest extends UnitTestCase {

  /**
   * The conductor helper service.
   *
   * @var \Drupal\conductor\Service\ConductorHelper|\PHPUnit\Framework\MockObject\MockObject
   */
  protected ConductorHelper|MockObject $conductorHelper;

  /**
   * The logger interface.
   *
   * @var \Psr\Log\LoggerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected LoggerInterface|MockObject $logger;

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList|\PHPUnit\Framework\MockObject\MockObject
   */
  protected ModuleExtensionList|MockObject $moduleExtension;

  /**
   * The renderer object.
   *
   * @var \Drupal\Core\Render\Renderer|\PHPUnit\Framework\MockObject\MockObject
   */
  protected Renderer|MockObject $renderer;

  /**
   * The html creator service.
   *
   * @var \Drupal\conductor\Service\Accordion\ConductorAccordionHTMLCreator
   */
  protected ConductorAccordionHTMLCreator $conductorAccordionHTMLCreator;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->conductorHelper = $this->createMock(ConductorHelper::class);
    $this->logger = $this->createMock(LoggerInterface::class);
    $this->moduleExtension = $this->createMock(ModuleExtensionList::class);
    $this->renderer = $this->createMock(Renderer::class);
    $this->conductorAccordionHTMLCreator = new ConductorAccordionHTMLCreator($this->conductorHelper,
      $this->logger, $this->moduleExtension, $this->renderer);

    // Mock the TranslationInterface.
    $translation = $this->createMock(TranslationInterface::class);
    $translation->method('translate')
      ->willReturnArgument(0);

    $this->conductorAccordionHTMLCreator->setStringTranslation($translation);
  }

  /**
   * Tests the constructor.
   *
   * @covers ::__construct
   */
  public function testConstructor(): void {

    $this->assertInstanceOf(ConductorAccordionHTMLCreator::class, $this->conductorAccordionHTMLCreator);
  }

  /**
   * Tests the constructGuidanceStatusClass method.
   */
  public function testConstructGuidanceStatusClass(): void {
    $data = [
      'status' => 'COMPLETED',
    ];

    $this->assertEquals('completed', $this->conductorAccordionHTMLCreator->constructGuidanceStatusClass($data));

    $data['insightsStatus'] = 'PROCESSING';

    $this->assertEquals('processing', $this->conductorAccordionHTMLCreator->constructGuidanceStatusClass($data));

  }

  /**
   * Tests the constructContentScore method returns valid content score.
   */
  public function testConstructContentScoreReturnsRenderedHtmlForValidContentScore(): void {
    $this->conductorHelper->method('convertToPercent')->willReturn('75%');
    $this->renderer->method('render')->willReturn('<div>Rendered HTML</div>');

    $details = ['content_score' => 75];
    $result = $this->conductorAccordionHTMLCreator->constructContentScore($details);

    $this->assertEquals('<div>Rendered HTML</div>', $result);
  }

  /**
   * Tests the constructScoreFactors method returns valid scores.
   */
  public function testConstructScoreFactorsReturnsRenderedHtmlForValidScores(): void {
    $this->conductorHelper->method('convertToPercent')->willReturnMap([
        [75, '75%'],
        [80, '80%'],
        [90, '90%'],
    ]);

    $this->renderer->method('render')->willReturn('<div>Rendered HTML</div>');

    $keywords = [
      'coverage_score' => 75,
      'natural_language_score' => 80,
      'repetition_score' => 90,
    ];
    $request_id = 'test_request_id';
    $result = $this->conductorAccordionHTMLCreator->constructScoreFactors($keywords, $request_id);

    $this->assertEquals('<div>Rendered HTML</div>', $result);
  }

  /**
   * Tests the Primary Keywords are correctly returned.
   */
  public function testConstructKeywordsInsightReturnsRenderedHtmlForPrimaryKeywords(): void {
    $this->conductorHelper->method('convertNumberToK')->willReturn('1K');
    $this->renderer->method('render')->willReturn('<div>Rendered HTML</div>');

    $keywords = [
      'details' => [
        'keywords' => [
          'items' => [
                      ['keyword' => 'test', 'msv' => 1000, 'ownRank' => TRUE],
          ],
        ],
      ],
    ];
    $result = $this->conductorAccordionHTMLCreator->constructKeywordsInsight('PRIMARY_KEYWORDS_INSIGHT', 'summary', $keywords, 'test_request_id');

    $this->assertEquals('<div>Rendered HTML</div>', $result);
  }

  /**
   * Tests the Secondary Keywords are correctly returned.
   */
  public function testConstructKeywordsInsightReturnsRenderedHtmlForSecondaryKeywords(): void {
    $this->conductorHelper->method('convertNumberToK')->willReturn('1K');
    $this->renderer->method('render')->willReturn('<div>Rendered HTML</div>');

    $keywords = [
      'details' => [
        'keywords' => [
          'items' => [
                      ['keyword' => 'test', 'msv' => 1000, 'ownRank' => TRUE],
          ],
        ],
      ],
    ];
    $result = $this->conductorAccordionHTMLCreator->constructKeywordsInsight('SECONDARY_KEYWORDS_INSIGHT', 'summary', $keywords, 'test_request_id');

    $this->assertEquals('<div>Rendered HTML</div>', $result);
  }

  /**
   * Tests the error messages are returned for no keywords.
   */
  public function testConstructKeywordsInsightReturnsErrorMessageForNoKeywords(): void {
    $this->conductorHelper->method('errorMessage')->willReturn('Error message');
    $this->renderer->method('render')->willReturn('<div>Error message</div>');

    $keywords = ['details' => ['keywords' => ['items' => []]]];
    $result = $this->conductorAccordionHTMLCreator->constructKeywordsInsight('PRIMARY_KEYWORDS_INSIGHT', 'summary', $keywords, 'test_request_id');

    $this->assertEquals('<div>Error message</div>', $result);
  }

  /**
   * Tests that more than five keywords are handled.
   */
  public function testConstructKeywordsInsightHandlesMoreThanFiveKeywords(): void {
    $this->conductorHelper->method('convertNumberToK')->willReturn('1K');
    $this->renderer->method('render')->willReturn('<div>Rendered HTML with see more/less</div>');

    $keywords = [
      'details' => [
        'keywords' => [
          'items' => array_fill(0, 6, ['keyword' => 'test', 'msv' => 1000, 'ownRank' => TRUE]),
        ],
      ],
    ];
    $result = $this->conductorAccordionHTMLCreator->constructKeywordsInsight('PRIMARY_KEYWORDS_INSIGHT', 'summary', $keywords, 'test_request_id');

    $this->assertEquals('<div>Rendered HTML with see more/less</div>', $result);
  }

  /**
   * Tests correct html is returned for valid keywords.
   */
  public function testConstructPatternsInsightReturnsRenderedHtmlForValidKeywords(): void {
    $this->renderer->method('render')->willReturn('<div>Rendered HTML</div>');

    $keywords = [
      'details' => [
        'keywords' => [
          'numOfSerps' => 10,
          'items' => [
                      ['keyword' => 'test', 'numOfOccurrence' => 5, 'ownMatched' => TRUE],
          ],
        ],
      ],
    ];
    $result = $this->conductorAccordionHTMLCreator->constructPatternsInsight('HTML_HEADING1_COMMON', 'summary', $keywords, 'test_request_id');

    $this->assertEquals('<div>Rendered HTML</div>', $result);
  }

  /**
   * Tests the error message is returned for no keywords.
   */
  public function testConstructPatternsInsightReturnsErrorMessageForNoKeywords(): void {
    $this->conductorHelper->method('errorMessage')->willReturn('Error message');
    $this->renderer->method('render')->willReturn('<div>Error message</div>');

    $keywords = ['details' => ['keywords' => ['items' => []]]];
    $result = $this->conductorAccordionHTMLCreator->constructPatternsInsight('HTML_HEADING1_COMMON', 'summary', $keywords, 'test_request_id');

    $this->assertEquals('<div>Error message</div>', $result);
  }

  /**
   * Tests that more than five keywords are handled correctly.
   */
  public function testConstructPatternsInsightHandlesMoreThanFiveKeywords(): void {
    $this->renderer->method('render')->willReturn('<div>Rendered HTML with see more/less</div>');

    $keywords = [
      'details' => [
        'keywords' => [
          'numOfSerps' => 10,
          'items' => array_fill(0, 6, ['keyword' => 'test', 'numOfOccurrence' => 5, 'ownMatched' => TRUE]),
        ],
      ],
    ];
    $result = $this->conductorAccordionHTMLCreator->constructPatternsInsight('HTML_HEADING1_COMMON', 'summary', $keywords, 'test_request_id');

    $this->assertEquals('<div>Rendered HTML with see more/less</div>', $result);
  }

  /**
   * Tests the constructContentOptimizationBodyCopy method returns valid html.
   */
  public function testConstructContentOptimizationBodyCopyReturnsRenderedHtmlForValidKeywords(): void {
    $this->renderer->method('render')->willReturn('<div>Rendered HTML</div>');

    $keywords = [
      'details' => [
        'keywords' => [
          'maxOccurrence' => 10,
          'items' => [
                      ['keyword' => 'test', 'numOfOccurrence' => 5, 'ownFrequency' => TRUE, 'presets' => ['IMPORTANT']],
          ],
        ],
        'predictiveFactors' => [
                  ['message' => 'Factor message'],
        ],
      ],
    ];
    $request_id = 'test_request_id';
    $result = $this->conductorAccordionHTMLCreator->constructContentOptimizationBodyCopy($keywords, $request_id);

    $this->assertEquals('<div>Rendered HTML</div>', $result);
  }

  /**
   * Tests the error message is returned for no keywords.
   */
  public function testConstructContentOptimizationBodyCopyReturnsErrorMessageForNoKeywords(): void {
    $this->conductorHelper->method('errorMessage')->willReturn('Error message');
    $this->renderer->method('render')->willReturn('<div>Error message</div>');

    $keywords = ['details' => ['keywords' => ['items' => []]]];
    $request_id = 'test_request_id';
    $result = $this->conductorAccordionHTMLCreator->constructContentOptimizationBodyCopy($keywords, $request_id);

    $this->assertEquals('<div>Error message</div>', $result);
  }

  /**
   * Tests that more than five keywords are handled correctly.
   */
  public function testConstructContentOptimizationBodyCopyHandlesMoreThanFiveKeywords(): void {
    $this->renderer->method('render')->willReturn('<div>Rendered HTML with see more/less</div>');

    $keywords = [
      'details' => [
        'keywords' => [
          'maxOccurrence' => 10,
          'items' => array_fill(0, 6,
            ['keyword' => 'test', 'numOfOccurrence' => 5, 'ownFrequency' => TRUE, 'presets' => ['IMPORTANT']]
          ),
        ],
        'predictiveFactors' => [
                  ['message' => 'Factor message'],
        ],
      ],
    ];
    $request_id = 'test_request_id';
    $result = $this->conductorAccordionHTMLCreator->constructContentOptimizationBodyCopy($keywords, $request_id);

    $this->assertEquals('<div>Rendered HTML with see more/less</div>', $result);
  }

  /**
   * Tests the constructContentOptimizationHeading method returns valid html.
   */
  public function testConstructReadabilityInsightReturnsRenderedHtmlForValidData(): void {
    $this->renderer->method('render')->willReturn('<div>Rendered HTML</div>');

    $keywords = [
      'details' => [
        'own' => ['metricValue' => ['label' => 'Draft Label']],
        'serp' => ['averageMetricValue' => ['label' => 'Average Label']],
      ],
    ];
    $result = $this->conductorAccordionHTMLCreator->constructReadabilityInsight('Summary', $keywords, 'test_request_id');

    $this->assertEquals('<div>Rendered HTML</div>', $result);
  }

  /**
   * Tests error message is returned for empty average metric value.
   */
  public function testConstructReadabilityInsightReturnsErrorMessageForEmptyAverage(): void {
    $this->conductorHelper->method('errorMessage')->willReturn('Error message');
    $this->renderer->method('render')->willReturn('<div>Error message</div>');

    $keywords = [
      'details' => [
        'own' => ['metricValue' => ['label' => 'Draft Label']],
        'serp' => ['averageMetricValue' => []],
      ],
    ];
    $result = $this->conductorAccordionHTMLCreator->constructReadabilityInsight('Summary', $keywords, 'test_request_id');

    $this->assertEquals('<div>Error message</div>', $result);
  }

  /**
   * Tests constructReadabilityInsight method handles missing own metric value.
   */
  public function testConstructReadabilityInsightHandlesMissingOwnMetricValue(): void {
    $this->renderer->method('render')->willReturn('<div>Rendered HTML</div>');

    $keywords = [
      'details' => [
        'own' => [],
        'serp' => ['averageMetricValue' => ['label' => 'Average Label']],
      ],
    ];
    $result = $this->conductorAccordionHTMLCreator->constructReadabilityInsight('Summary', $keywords, 'test_request_id');

    $this->assertEquals('<div>Rendered HTML</div>', $result);
  }

  /**
   * Tests the constructContentLength method returns valid html.
   */
  public function testConstructContentLengthReturnsRenderedHtmlForValidData(): void {
    $this->renderer->method('render')->willReturn('<div>Rendered HTML</div>');

    $keywords = [
      'details' => [
        'own' => ['metricValue' => 1500],
        'serp' => ['averageMetricValue' => 1200],
        'predictiveFactors' => [
                  ['message' => 'Factor message'],
        ],
      ],
    ];
    $result = $this->conductorAccordionHTMLCreator->constructContentLength($keywords, 'test_request_id');

    $this->assertEquals('<div>Rendered HTML</div>', $result);
  }

  /**
   * Tests error message is returned for empty average metric value.
   */
  public function testConstructContentLengthReturnsErrorMessageForEmptyAverageMetricValue(): void {
    $this->conductorHelper->method('errorMessage')->willReturn('Error message');
    $this->renderer->method('render')->willReturn('<div>Error message</div>');

    $keywords = [
      'details' => [
        'own' => ['metricValue' => 1500],
        'serp' => ['averageMetricValue' => []],
        'predictiveFactors' => [
                  ['message' => 'Factor message'],
        ],
      ],
    ];
    $result = $this->conductorAccordionHTMLCreator->constructContentLength($keywords, 'test_request_id');

    $this->assertEquals('<div>Error message</div>', $result);
  }

  /**
   * Tests constructContentLength method handles missing own metric value.
   */
  public function testConstructContentLengthHandlesMissingOwnMetricValue(): void {
    $this->renderer->method('render')->willReturn('<div>Rendered HTML</div>');

    $keywords = [
      'details' => [
        'own' => [],
        'serp' => ['averageMetricValue' => 1200],
        'predictiveFactors' => [
                  ['message' => 'Factor message'],
        ],
      ],
    ];
    $result = $this->conductorAccordionHTMLCreator->constructContentLength($keywords, 'test_request_id');

    $this->assertEquals('<div>Rendered HTML</div>', $result);
  }

  /**
   * Tests the constructScoreFactors method returns valid scores.
   */
  public function testsConstructScoreFactorsReturnsRenderedHtmlForValidScores(): void {
    $this->renderer->method('render')->willReturn('<div>Rendered HTML</div>');

    $keywords = [
      'coverage_score' => 80,
      'natural_language_score' => 90,
      'repetition_score' => 70,
    ];
    $request_id = 'test_request_id';
    $result = $this->conductorAccordionHTMLCreator->constructScoreFactors($keywords, $request_id);

    $this->assertEquals('<div>Rendered HTML</div>', $result);
  }

  /**
   * Tests the ConstructScoreFactors method returns error message appropriately.
   */
  public function testsConstructScoreFactorsReturnsErrorMessageForEmptyScores(): void {
    $this->renderer->method('render')->willReturn('<div>Error message</div>');

    $keywords = [];
    $request_id = 'test_request_id';
    $result = $this->conductorAccordionHTMLCreator->constructScoreFactors($keywords, $request_id);

    $this->assertEquals('<div>Error message</div>', $result);
  }

  /**
   * Tests the ConstructScoreFactors method handles missing scores.
   */
  public function testsConstructScoreFactorsHandlesMissingCoverageScore(): void {
    $this->renderer->method('render')->willReturn('<div>Rendered HTML</div>');

    $keywords = [
      'natural_language_score' => 90,
      'repetition_score' => 70,
    ];
    $request_id = 'test_request_id';
    $result = $this->conductorAccordionHTMLCreator->constructScoreFactors($keywords, $request_id);

    $this->assertEquals('<div>Rendered HTML</div>', $result);
  }

  /**
   * Tests the ConstructScoreFactors method handles missing score.
   */
  public function testsConstructScoreFactorsHandlesMissingNaturalLanguageScore(): void {
    $this->renderer->method('render')->willReturn('<div>Rendered HTML</div>');

    $keywords = [
      'coverage_score' => 80,
      'repetition_score' => 70,
    ];
    $request_id = 'test_request_id';
    $result = $this->conductorAccordionHTMLCreator->constructScoreFactors($keywords, $request_id);

    $this->assertEquals('<div>Rendered HTML</div>', $result);
  }

  public function constructScoreFactorsHandlesMissingRepetitionScore(): void {
    $this->renderer->method('render')->willReturn('<div>Rendered HTML</div>');

    $keywords = [
      'coverage_score' => 80,
      'natural_language_score' => 90,
    ];
    $request_id = 'test_request_id';
    $result = $this->conductorAccordionHTMLCreator->constructScoreFactors($keywords, $request_id);

    $this->assertEquals('<div>Rendered HTML</div>', $result);
  }

}
