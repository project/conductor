<?php

declare(strict_types=1);

namespace Drupal\Tests\conductor\Unit\Service\APIConnector;

use Drupal\Component\Render\MarkupInterface;
use Drupal\conductor\Service\Accordion\ConductorAccordionDataMapper;
use Drupal\conductor\Service\Accordion\ConductorAccordionHTMLCreator;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Tests\UnitTestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @coversDefaultClass \Drupal\conductor\Service\Accordion\ConductorAccordionDataMapper
 * @group conductor
 */
class ConductorAccordionDataMapperTest extends UnitTestCase {

  /**
   * Conductor HTML Creator.
   *
   * @var \PHPUnit\Framework\MockObject\MockObject|\Drupal\conductor\Service\Accordion\ConductorAccordionHTMLCreator
   */
  protected MockObject|ConductorAccordionHTMLCreator $conductorHTMLCreator;

  /**
   * Conductor data mapper.
   *
   * @var \Drupal\conductor\Service\Accordion\ConductorAccordionDataMapper
   */
  protected ConductorAccordionDataMapper $conductorAccordionDataMapper;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->conductorHTMLCreator = $this->createMock(ConductorAccordionHTMLCreator::class);

    $this->conductorAccordionDataMapper = new ConductorAccordionDataMapper($this->conductorHTMLCreator);
  }

  /**
   * Tests the constructor.
   *
   * @covers ::__construct
   */
  public function testConstructor(): void {
    $this->assertInstanceOf(ConductorAccordionDataMapper::class, $this->conductorAccordionDataMapper);
  }

  /**
   * Tests map data.
   *
   * @throws \Exception
   */
  public function testMapData(): void {

    $response = new AjaxResponse();
    $data =
      [
        "status" => "COMPLETED",
        "insightsStatus" => "COMPLETED",
        "snippetsStatus" => "COMPLETED",
        "startTime" => "1723574861788",
        "progress" =>
        [
          "total" => 15,
          "completed" => 15,
          "error" => 0,
        ],
        "insightsProgress" =>
        [
          "total" => 14,
          "completed" => 14,
          "error" => 0,
        ],
        "snippetsProgress" => [
          "total" => 1,
          "completed" => 1,
          "error" => 0,
        ],
        // "requests" => Array(),
        "results" => [
          "snippets" => [
            0 => [
              "details" =>
              [
                "repetition_score" => 0.00014046639231824,
                "natural_language_score" => 0.91575925712986,
                "coverage_score" => 0.19980299448385,
                "content_score" => 0.42271021725636,
              ],
              "type" => "CONTENT_SCORE_SNIPPET",
              "status" => "COMPLETED",
            ],

          ],
          "insights" => [
            0 => [
              "type" => "HTML_TITLE_COMMON",
              "status" => "COMPLETED",
            ],
            1 => [
              "type" => "CONTENT_ANALYSIS_INSIGHT",
              "status" => "COMPLETED",
            ],
            2 => [
              "type" => "QUESTIONS_INSIGHT_V2",
              "status" => "COMPLETED",
            ],
          ],
        ],
      ];

    $render_html = $this->createMock(MarkupInterface::class);

    $this->conductorHTMLCreator->expects($this->once())
      ->method('constructContentOptimizationBodyCopy')
      ->willReturn($render_html);

    $this->conductorHTMLCreator->expects($this->once())
      ->method('constructQuestions')
      ->willReturn($render_html);

    $this->conductorHTMLCreator->expects($this->once())
      ->method('constructPatternsInsight')
      ->willReturn($render_html);

    $this->conductorHTMLCreator->expects($this->once())
      ->method('constructContentScore')
      ->willReturn($render_html);

    $this->conductorHTMLCreator->expects($this->once())
      ->method('constructScoreFactors')
      ->willReturn($render_html);

    $this->conductorHTMLCreator->expects($this->once())
      ->method('constructGuidanceStatusClass')
      ->willReturn($render_html);

    $this->conductorAccordionDataMapper->mapData($response, $data, '1234');

  }

  /**
   * Tests set progress value handles completed status.
   */
  public function testSetProgressValueHandlesCompletedStatus(): void {

    $response = $this->createMock(AjaxResponse::class);
    $response->expects($this->exactly(2))
      ->method('addCommand')
      ->willReturn($response);

    $data = ['status' => 'COMPLETED', 'progress' => ['completed' => 5, 'total' => 5]];

    $result = $this->conductorAccordionDataMapper->setProgressValue($response, $data);

    $this->assertInstanceOf(AjaxResponse::class, $result);
  }

  /**
   * Tests set progress value handles processing status.
   */
  public function testSetProgressValueHandlesProcessingStatus(): void {

    $response = $this->createMock(AjaxResponse::class);
    $response->expects($this->exactly(2))
      ->method('addCommand')
      ->willReturn($response);
    $data = ['status' => 'PROCESSING', 'progress' => ['completed' => 3, 'total' => 5]];

    $result = $this->conductorAccordionDataMapper->setProgressValue($response, $data);

    $this->assertInstanceOf(AjaxResponse::class, $result);
  }

  /**
   * Tests get steps reset functionality.
   */
  public function testGetStepsReturnsZeroOfZeroForCompletedStatus(): void {
    $data = ['status' => 'COMPLETED', 'progress' => ['completed' => 5, 'total' => 5]];
    $result = $this->conductorAccordionDataMapper->getSteps($data);
    $this->assertEquals('0 of 0 steps completed..', $result);
  }

  /**
   * Tests get steps returns completed steps.
   */
  public function testGetStepsReturnsCompletedSteps(): void {
    $data = ['status' => 'PROCESSING', 'progress' => ['completed' => 3, 'total' => 5]];

    $result = $this->conductorAccordionDataMapper->getSteps($data);
    $this->assertEquals('3 of 5 steps completed..', $result);
  }

}
