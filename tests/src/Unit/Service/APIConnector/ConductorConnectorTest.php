<?php

declare(strict_types=1);

namespace Drupal\Tests\conductor\Unit\Service\APIConnector;

use Drupal\conductor\Exception\ConductorApiRequestException;
use Drupal\conductor\Service\APIConnector\ConductorConnector;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;

/**
 * @coversDefaultClass \Drupal\conductor\Service\APIConnector\ConductorConnector
 * @group conductor
 */
class ConductorConnectorTest extends UnitTestCase {

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $configSettings;

  /**
   * Guzzle HTTP client.
   *
   * @var \GuzzleHttp\Client|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $httpClient;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $logger;

  /**
   * Conductor Connector service.
   *
   * @var \Drupal\conductor\Service\APIConnector\ConductorConnector|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $connectorService;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->httpClient = $this->createMock(Client::class);
    $this->logger = $this->createMock('\Psr\Log\LoggerInterface');
    $this->configSettings = $this->getConfigFactoryStub([
      'conductor.settings' => [
        'conductor' => [
          'api_key' => '',
          'shared_secret' => '',
        ],
      ],
    ]);
    $this->connectorService = new ConductorConnector($this->configSettings, $this->httpClient, $this->logger);
  }

  /**
   * Tests connection to account.
   */
  public function testConnectorErrorHandling(): void {
    // Http client throws an exception.
    $this->httpClient
      ->method('request')
      ->willThrowException(new \Exception());

    // Exception is caught and re thrown.
    $this->expectException(ConductorApiRequestException::class);

    $this->connectorService->request('GET', '/accounts', [], 'api_key', 'shared_secret');

  }

  /**
   * Tests connector is working.
   */
  public function testConnectorIsWorking(): void {
    try {
      // Http client returns a response.
      $this->httpClient
        ->method('request')
        ->willReturn(new Response(200, [], ''));

      $response = $this->connectorService->request('GET', '/accounts');
      $this->assertIsArray($response);
    }
    catch (\Exception $e) {
      $this->fail('Exception thrown when it should not have been .');
    }

  }

}
