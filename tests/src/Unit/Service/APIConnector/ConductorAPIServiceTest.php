<?php

declare(strict_types=1);

namespace Drupal\Tests\conductor\Unit\Service\APIConnector;

use Drupal\conductor\Exception\ConductorApiRequestException;
use Drupal\conductor\Service\APIConnector\ConductorAPIService;
use Drupal\conductor\Service\APIConnector\ConductorConnector;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Psr\Log\LoggerInterface;

/**
 * @coversDefaultClass \Drupal\conductor\Service\APIConnector\ConductorAPIService
 * @group conductor
 */
class ConductorAPIServiceTest extends UnitTestCase {

  /**
   * Conductor API Service.
   *
   * @var \Drupal\conductor\Service\APIConnector\ConductorAPIService|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $conductorAPIService;

  /**
   * Conductor Connector service.
   *
   * @var \Drupal\conductor\Service\APIConnector\ConductorConnector|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $connectorService;

  /**
   * Guzzle HTTP client.
   *
   * @var \GuzzleHttp\Client|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $httpClient;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $logger;

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $configSettings;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->httpClient = $this->createMock(Client::class);
    $this->configSettings = $this->getConfigFactoryStub([
      'conductor.settings' => [
        'conductor' => [
          'api_key' => '',
          'shared_secret' => '',
          'content_types' => [
            'article',
          ],
        ],
      ],
    ]);
    $this->logger = $this->createMock(LoggerInterface::class);
    $this->connectorService = new ConductorConnector($this->configSettings, $this->httpClient, $this->logger);
    $this->conductorAPIService = new ConductorAPIService($this->connectorService, $this->configSettings, $this->logger);
  }

  /**
   * Tests fetching recommendations.
   */
  public function testFetchRecommendations() {
    $this->httpClient
      ->method('request')
      ->willReturn(new Response(200, [], json_encode([['recommendations' => 'Sample Data']])));
    // Fetch recommendations.
    $recommendations = $this->conductorAPIService->fetchRecommendations('account_id', 'request_id', $override_api_key = '', $override_shared_secret = '');

    $this->assertIsArray($recommendations);

    // Test exceptions thrown are bubbling up.
    $this->httpClient
      ->method('request')
      ->willThrowException(new ConductorApiRequestException());

    $this->expectException(ConductorApiRequestException::class);

    $this->conductorAPIService->fetchRecommendations('account_id', 'request_id', $override_api_key = '', $override_shared_secret = '');

  }

  /**
   * Tests fetching the request ID.
   */
  public function testFetchRequestId() {
    $this->httpClient
      ->method('request')
      ->willReturn(new Response(200, [], json_encode(['request_id' => '1234'])));
    // Fetch request ID.
    $request_id = $this->conductorAPIService->fetchRequestId('account_id', 'request_id', 'topic', 1, $url = '', $body = '', $override_api_key = '', $override_shared_secret = '');
    // Request will return an empty array unless provided a valid api key and
    // shared secret.
    $this->assertEquals('1234', $request_id);

    // Test exceptions thrown are bubbling up.
    $this->httpClient
      ->method('request')
      ->willThrowException(new ConductorApiRequestException());

    $this->expectException(ConductorApiRequestException::class);

    $this->conductorAPIService->fetchRequestId('account_id', 'request_id', 'topic', 1, $url = '', $body = '', $override_api_key = '', $override_shared_secret = '');

  }

  /**
   * Tests getting account ID.
   */
  public function testGetAccountId(): void {
    $this->httpClient
      ->method('request')
      ->willReturn(new Response(200, [], json_encode([['accountId' => '1234']])));
    // Get account ID.
    $account_id = $this->conductorAPIService->getAccountId();
    // Assert data returned is correctly parsed.
    $this->assertEquals('1234', $account_id);

    // Test exceptions thrown are bubbling up.
    $this->httpClient
      ->method('request')
      ->willThrowException(new ConductorApiRequestException());

    $this->expectException(ConductorApiRequestException::class);

    $this->conductorAPIService->getAccountId();

  }

  /**
   * Tests get rank sources.
   */
  public function testGetRankSources(): void {
    // Get rank resources.
    $rank_resources = $this->conductorAPIService->getRankSources();
    // Request will return an empty array unless provided a valid api key and
    // shared secret.
    $this->assertEmpty($rank_resources);

    // Test exceptions thrown are handled.
    $this->httpClient
      ->method('request')
      ->willThrowException(new ConductorApiRequestException());

    // We expect the getRankSources method NOT to throw an exception.
    try {
      $this->conductorAPIService->getRankSources();
    }
    catch (\Exception $e) {
      // If an exception is caught, the test should fail.
      $this->fail('An exception was thrown when it should not have been.');
    }

  }

  /**
   * Tests empty rank sources are returned for empty account Id.
   */
  public function testGetRankSourcesReturnsEmptyArrayForEmptyAccountId(): void {
    $conductorConnector = $this->createMock(ConductorConnector::class);
    $conductorConnector->method('request')->willReturn([]);
    $configFactory = $this->createMock(ConfigFactoryInterface::class);
    $logger = $this->createMock(LoggerInterface::class);

    $service = new ConductorAPIService($conductorConnector, $configFactory, $logger);
    $result = $service->getRankSources();

    $this->assertEquals([], $result);
  }

  /**
   * Tests getting web property ID.
   */
  public function testGetWebPropertyId(): void {
    $this->httpClient
      ->method('request')
      ->willReturn(new Response(200, [], json_encode([['webPropertyId' => '6789']])));
    // Get Web Property ID.
    $web_property_id = $this->conductorAPIService->getWebPropertyId('1234');
    // Assert data returned is correctly parsed.
    $this->assertEquals('6789', $web_property_id);

    // Test exceptions thrown are bubbling up.
    $this->httpClient
      ->method('request')
      ->willThrowException(new ConductorApiRequestException());

    $this->expectException(ConductorApiRequestException::class);

    $this->conductorAPIService->getWebPropertyId('account_id');

  }

  /**
   * Tests function generates new request id.
   *
   * @covers ::getRequestId
   */
  public function testGetRequestIdGeneratesNewRequestId(): void {
    $this->httpClient
      ->method('request')
      ->willReturn(new Response(200, [], json_encode(['request_id' => 1234, 'status' => 'SUBMITTED'])));
    $formState = $this->createMock(FormStateInterface::class);
    $formState->method('getValue')->willReturnMap([
      ['topic', 'new_topic'],
      ['request_id', ''],
    ]);

    $result = $this->conductorAPIService->getRequestId(1, 1, $this->configSettings, 'body_copy', $formState);
    $this->assertEquals(1234, $result);
  }

  /**
   * Tests function returns empty for invalid request id.
   *
   * @covers ::getRequestId
   */
  public function testGetRequestIdReturnsEmptyForInvalidRequestId(): void {
    $this->httpClient
      ->method('request')
      ->willReturn(new Response(200, [], json_encode([])));
    $formState = $this->createMock(FormStateInterface::class);
    $formState->method('getValue')->willReturnMap([
      ['topic', 'invalid_topic'],
      ['request_id', ''],
    ]);

    $result = $this->conductorAPIService->getRequestId(1, 1, $this->configSettings, 'body_copy', $formState);
    $this->assertEquals('', $result);
  }

}
