<?php

namespace Drupal\Tests\conductor\Kernel;

use Drupal\conductor\Service\Accordion\ConductorAccordionDataMapper;
use Drupal\conductor\Service\APIConnector\ConductorAPIService;
use Drupal\conductor\Service\DataPreProcessor\ConductorNodeDataPreProcessor;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Form\FormStateInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;

/**
 * Tests that a node is not saved under certain conditions.
 *
 * @group my_module
 */
class ConductorGenerateRecommendationsTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'user',
    'system',
    'field',
    'text',
    'filter',
    'conductor',
  ];

  /**
   * Sets up the environment for the test.
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installSchema('node', ['node_access']);
    $this->installConfig(['node']);

    // Create a content type.
    NodeType::create(['type' => 'article'])->save();

    $conductor_api_service = $this->createMock(ConductorAPIService::class);
    $conductor_api_service->expects($this->once())
      ->method('getAccountId')
      ->willReturn('123');

    $conductor_api_service->expects($this->once())
      ->method('getWebPropertyId')
      ->willReturn('345');

    $conductor_api_service->expects($this->once())
      ->method('getRequestId')
      ->willReturn('678');

    $conductor_api_service->expects($this->once())
      ->method('fetchRecommendations')
      ->willReturn('[{"title":"Sample Topic","body":"Sample Topic"}]');

    $this->container->set('conductor.api_service', $conductor_api_service);

    $data_mapper = $this->createMock(ConductorAccordionDataMapper::class);
    $data_mapper->expects($this->once())
      ->method('mapData')
      ->willReturn(new AjaxResponse());
    $this->container->set('conductor.accordion_data_mapper', $data_mapper);

    $pre_processor = $this->createMock(ConductorNodeDataPreProcessor::class);
    $pre_processor->expects($this->once())
      ->method('getBodyCopy')
      ->willReturn('New Article');
    $this->container->set('conductor.node_data_pre_processor', $pre_processor);
  }

  /**
   * Tests node is not saved when generating recommendations for new nodes.
   *
   * @throws \Exception
   */
  public function testNodeNotSavedWhenGeneratingRecommendationsForNewNodes(): void {
    try {
      // Apply the conditions, here we just prepare a node.
      $node = Node::create([
        'type' => 'article',
        'title' => 'New Article',
      ]);
      $node->setNewRevision();

      $form = [];
      $form_state = $this->createMock(FormStateInterface::class);
      $form_state->expects($this->atLeastOnce())
        ->method('getValue')
        ->willReturn('Sample Topic');

      $this->container->get('conductor.manager')
        ->buildAccordion($form, $form_state, new AjaxResponse());

      $node_revisions_count = \Drupal::entityQuery('node')
        ->condition('title', 'New Article')
        ->allRevisions()
        ->count()
        ->accessCheck(TRUE)
        ->execute();
      $this->assertEquals(0, $node_revisions_count, 'No nodes with the title "New Article" were found.');
    }
    catch (\Exception $e) {
      $this->fail($e->getMessage());
    }
  }

  /**
   * Tests node is not saved when generating recommendations for existing nodes.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Exception
   */
  public function testNodeNotSavedWhenGeneratingRecommendationsForExistingNodes(): void {
    // Apply the conditions, here we just prepare a node.
    $node = Node::create([
      'type' => 'article',
      'title' => 'New Article',
    ]);

    $node->save();

    // Update the node and run analysis.
    $node->setTitle('New Updated Title');
    $node->setNewRevision();

    $form = [];
    $form_state = $this->createMock(FormStateInterface::class);
    $form_state->expects($this->atLeastOnce())
      ->method('getValue')
      ->willReturn('Sample Topic');

    $this->container->get('conductor.manager')
      ->buildAccordion($form, $form_state, new AjaxResponse());

    $node_revisions_count = \Drupal::entityQuery('node')
      ->condition('uuid', $node->uuid())
      ->allRevisions()
      ->count()
      ->accessCheck(TRUE)
      ->execute();
    $this->assertEquals(1, $node_revisions_count, 'Only 1 node revision were found.');
  }

}
