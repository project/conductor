<?php

namespace Drupal\Tests\conductor\Functional;

use Drupal\conductor\Exception\ConductorApiRequestException;
use Drupal\Tests\BrowserTestBase;
use GuzzleHttp\Client;

/**
 * Tests the admin configuration form.
 *
 * @group conductor
 */
class ConductorConfigFormTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['node', 'conductor'];


  /**
   * The default theme to use.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->drupalLogin($this->drupalCreateUser(['administer conductor']));

  }

  /**
   * Tests that the config form is accessible.
   */
  public function testFormAccess(): void {
    $this->drupalGet('/admin/config/development/conductor');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Test that the config form is accessible when Conductor API is down.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testFormAccessWhenConductorApiIsDown(): void {
    $container = \Drupal::getContainer();
    $container->set('http_client', $this->createMock(Client::class)
      ->method('request')
      ->willThrowException(new ConductorApiRequestException())
    );
    \Drupal::setContainer($container);

    $this->drupalGet('/admin/config/development/conductor');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Test form submission with random API key.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testFormSubmissionFailsWithRandomApiKey(): void {
    $this->drupalGet('/admin/config/development/conductor');

    $this->getSession()->getPage()->findField('api_key')->setValue('Sample API Key');
    $this->getSession()->getPage()->findField('shared_secret')->setValue('Sample shared secret');
    $this->getSession()->getPage()->pressButton('Save configuration');

    $this->assertSession()->pageTextContains('Unable to connect to Conductor API.');

  }

  /**
   * Tests logging is disabled by default.
   */
  public function testLoggingDisabledByDefault(): void {
    $this->drupalGet('/admin/config/development/conductor');
    $this->assertEquals('0', $this->getSession()->getPage()->findField('logging')->getValue());
  }

  /**
   * Test if config values are reflecting in form.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testConfigSettingsMatchFormDefaultValues(): void {
    $config = \Drupal::configFactory()->getEditable('conductor.settings');
    $config->set('conductor.logging', '1');
    $config->set('conductor.api_key', 'Sample API key');
    $config->set('conductor.shared_secret', 'Sample shared secret');
    $config->save();

    $this->drupalLogin($this->drupalCreateUser(['administer conductor']));
    $this->drupalGet('/admin/config/development/conductor');

    $this->assertEquals('1', $this->getSession()->getPage()->findField('logging')->getValue());
    $this->assertEquals('Sample API key', $this->getSession()->getPage()->findField('api_key')->getValue());
    $this->assertEquals('Sample shared secret', $this->getSession()->getPage()->findField('shared_secret')->getValue());

  }

  /**
   * Test Rank Sources is not visible when Api is down.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function testRankSourcesIsNotVisibleWhenApiIsDown(): void {
    $container = \Drupal::getContainer();
    $container->set('http_client', $this->createMock(Client::class)
      ->method('request')
      ->willThrowException(new ConductorApiRequestException())
    );
    \Drupal::setContainer($container);

    $this->drupalGet('/admin/config/development/conductor');
    $this->assertSession()->pageTextNotContains('Rank Sources');
  }

}
