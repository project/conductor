<?php

namespace Drupal\Tests\conductor\Functional;

use Drupal\node\Entity\Node;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the Conductor Helper build preview.
 *
 * @group conductor
 */
class ConductorHelperBuildPreviewTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['node', 'conductor', 'conductor_test'];

  /**
   * The default theme to use.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * The profile to install as a basis for testing.
   *
   * @var string
   */
  protected $profile = 'standard';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Enabling article content type for Conductor analysis.
    \Drupal::service('config.factory')->getEditable('conductor.settings')
      ->set('conductor.content_types', ['article', 'page'])
      ->save();
  }

  /**
   * Test the build preview method.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testBuildPreview(): void {
    // Creating a node of type article.
    $node = Node::create([
      'type' => 'article',
      'title' => 'Test rendered title',
    ]);
    $node->save();

    $body_copy = \Drupal::service('conductor.node_data_pre_processor')->buildPreview($node);
    $this->assertStringNotContainsString('<h2>', $body_copy);
    $this->assertStringContainsString('<h1>', $body_copy);
  }

  /**
   * Test the build preview method with template overrides.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testBuildPreviewWithTemplateOverrides(): void {
    // Creating a node of type article.
    $node = Node::create([
      'type' => 'article',
      'title' => 'Test rendered title',
    ]);
    $node->save();

    $body_copy = \Drupal::service('conductor.node_data_pre_processor')->buildPreview($node);
    // Test node title can be overridden.
    $this->assertStringNotContainsString('Test rendered title', $body_copy);
    // Test if node preprocess hook is used.
    $this->assertStringContainsString('Title from preprocess hook.', $body_copy);
    // Test if custom template is used.
    $this->assertStringContainsString('Custom template in action.', $body_copy);

  }

  public function testBuildPreviewWithScriptTags(): void {
    // Creating a node of type article.
    $node = Node::create([
      'type' => 'article',
      'title' => 'Test rendered title',
      'body' => 'Content with <script>alert("Hello World!");</script>',
    ]);
    $node->save();

    $body_copy = \Drupal::service('conductor.node_data_pre_processor')->buildPreview($node);

    $this->assertStringNotContainsString('<script>alert', $body_copy);
  }

}
