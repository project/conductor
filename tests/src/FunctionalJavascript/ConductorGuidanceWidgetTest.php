<?php

namespace Drupal\Tests\conductor\FunctionalJavascript;

use Drupal\conductor\Exception\ConductorApiRequestException;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use GuzzleHttp\Client;

/**
 * Tests the Conductor Guidance Widget.
 *
 * @group conductor
 */
class ConductorGuidanceWidgetTest extends WebDriverTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['node', 'conductor', 'user'];

  /**
   * The default theme to use.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * The profile to install as a basis for testing.
   *
   * @var string
   */
  protected $profile = 'standard';

  /**
   * A user with the 'access content' permission.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();

    // Create new article content.
    $this->user = $this->drupalCreateUser([
      'create article content',
    ]);

    $this->drupalLogin($this->user);

    // Enabling article content type for Conductor analysis.
    \Drupal::service('config.factory')->getEditable('conductor.settings')
      ->set('conductor.content_types', ['article', 'page'])
      ->save();

  }

  /**
   * Tests widget is available.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testWidgetIsAvailable(): void {

    $container = \Drupal::getContainer();
    $container->set('http_client', $this->createMock(Client::class)
      ->method('request')
      ->willThrowException(new ConductorApiRequestException())
    );
    \Drupal::setContainer($container);

    try {
      $this->drupalGet('/node/add/article');

      $page = $this->getSession()->getPage();
      $page->fillField('title[0][value]', 'Test Article');
      $page->clickLink('Conductor');

      $this->assertSession()->pageTextContains('Writing Assistant');

      $page->fillField('topic', 'Topic1');
      $page->pressButton('Get guidance');
    }
    catch (\Exception $e) {
      $this->fail('An exception was thrown when it should not have been.');
    }

  }

}
