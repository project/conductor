## Conductor API Documentation

The Conductor API provides secure, programmatic access to data in Conductor accounts.
This API allows organizations to receive data about the current configuration
of their accounts and to request reports related to those accounts.
You can access the Conductor API through https requests to the endpoints
listed in this document and described in full in our interactive documentation
found at developers.conductor.com. Where necessary, work with Conductor users at your organization
to help guide you through understanding the Conductor-specific concepts described in this document.

## API
Access the API endpoint using: `[url]/api/v1/content-guidance`. The endpoint accepts the following parameters:
- api_key
- shared_secret
- url
- body_copy
- rank_source_id
- topic
