/**
 * @file
 * Global utilities.
 */
(function ($, Drupal) {
  // Helper function to toggle the 'collapse' class.
  function toggleCollapse() {
    this.parentNode.classList.toggle('collapse');
  }

  // Helper function to check for the Enter key and toggle the 'collapse' class.
  function keyToggleCollapse(e) {
    if (e.keyCode === 13) {
      this.parentNode.classList.toggle('collapse');
    }
  }
  // Remove Accordion events.
  function removeAccordionEvents() {
    // Get all accordion elements.
    const accordions = document.querySelectorAll('.conductor__accordion');

    // Iterate over each accordion element.
    accordions.forEach(function (accordion) {
      // Find all accordion labels within this accordion.
      const labels = accordion.querySelectorAll('.conductor__accordion-label');

      labels.forEach(function (label) {
        // Remove any previous click event listeners.
        label.removeEventListener('click', toggleCollapse);
        // Remove any previous keyup event listeners.
        label.removeEventListener('keyup', keyToggleCollapse);
      });
    });
  }

  // Set Accordion events.
  function setAccordionEvents() {
    // Get all accordion elements.
    const accordions = document.querySelectorAll('.conductor__accordion');

    // Iterate over each accordion element.
    accordions.forEach(function (accordion) {
      // Find all accordion labels within this accordion.
      const labels = accordion.querySelectorAll('.conductor__accordion-label');

      labels.forEach(function (label) {
        // Add the new click event listener.
        label.addEventListener('click', toggleCollapse);
        // Add the new keyup event listener.
        label.addEventListener('keyup', keyToggleCollapse);
      });
    });
  }

  // See more/less events.
  function seeMoreLess(event) {
    event.preventDefault();
    $(this)
      .parents('table')
      .find('.hidden-row')
      .each(function () {
        $(this).toggleClass('hidden');
      });
    $(this)
      .parents('table')
      .find('.see-more')
      .each(function () {
        $(this).toggleClass('hidden');
      });
    $(this)
      .parents('table')
      .find('.see-less')
      .each(function () {
        $(this).toggleClass('hidden');
      });
  }

  Drupal.behaviors.conductor = {
    attach(context, settings) {
      if ($('.conductor-config-form').length > 0) {
        // Implement Select2 on select lists.
        $('.conductor-config-form select').each(function () {
          $(this).select2();
        });
      }

      // Node block.
      if ($('#conductor-form').length > 0) {
        // Keyword input.
        if (
          document.querySelector(
            '.conductor__keyword-container input[type="text"]',
          ).value !== ''
        ) {
          const parent = $('#conductor-form').find(
            '.conductor__keyword-container',
          );
          parent.addClass('has-content');
          parent.find('input[type="submit"]').removeAttr('disabled');
          parent.find('input[type="submit"]').removeClass('is-disabled');
        }
        $('.conductor__keyword-container input[type="text"]').keyup(
          function () {
            const parent = $(this).parents('.conductor__keyword-container');
            if (this.value !== '') {
              parent.addClass('has-content');
              parent.find('input[type="submit"]').removeAttr('disabled');
              parent.find('input[type="submit"]').removeClass('is-disabled');
            } else {
              parent.removeClass('has-content');
              parent.find('input[type="submit"]').attr('disabled', 'disabled');
              parent.find('input[type="submit"]').addClass('is-disabled');
            }
          },
        );

        // Reset error message.
        $('.conductor__keyword-container input[type="submit"]').on(
          'mousedown click keyup',
          function (e) {
            $('#conductor-form')
              .find('.conductor__message .conductor__error')
              .parent()
              .html('');
          },
        );

        // Ajax observer.
        const observer = new MutationObserver((mutations) => {
          $(mutations).each((key, mutation) => {
            // Sift through added nodes in the DOM.
            const nodes = Array.prototype.slice.call(mutation.addedNodes);
            nodes.forEach((node) => {
              const preloader = $('#conductor-form').find(
                '.conductor__preloader',
              );
              const submitButton = $('#conductor-form').find(
                '.conductor__keyword-container input[type="submit"]',
              );
              const progressBar = $('#conductor-form').find('.progress-bar');
              // Keyword search.
              // Check for ajax progress throbber and add generating logo.
              if ($(node).hasClass('ajax-progress--throbber')) {
                submitButton.val('Refresh guidance');
                if (
                  !preloader.hasClass('generating') ||
                  !submitButton.hasClass('generating')
                ) {
                  preloader.removeClass('generated');
                  preloader.addClass('generating');
                  submitButton.addClass('generating');
                }
              }
              // Remove generating logo.
              if ($(node).hasClass('completed') || $(node).hasClass('failed')) {
                preloader.removeClass('generating');
                preloader.addClass('generated');
                submitButton.removeAttr('disabled');
                submitButton.removeClass('is-disabled generating');
                progressBar.html('0%');
                progressBar.css('width', '0%');
                removeAccordionEvents();
                setAccordionEvents();
                if (
                  !$('.conductor__content-optimization').hasClass('collapse')
                ) {
                  $('.conductor__content-optimization').addClass('collapse');
                }
                // Help text adjustment.
                $('.conductor__help-text__container').each(function () {
                  const offset = $(this).find('img').position().left;
                  if (offset >= 140) {
                    $(this)
                      .find('.conductor__help-text__popup')
                      .addClass('left');
                  }
                });
              }
              // Refresh guidance automatically every 10 seconds.
              if (
                $(node).hasClass('conductor__message-inner') &&
                $(node).hasClass('processing')
              ) {
                setTimeout(function () {
                  $('#conductor-form')
                    .find('.conductor__keyword-container input[type="submit"]')
                    .mousedown();
                }, 10000);
              }
              // Set see more function.
              // 'node' is assumed to be a DOM element.
              if ($(node).hasClass('conductor__message-inner')) {
                // Find all '.see-more' buttons within '#conductor-form' and add 'click' event listeners.
                document
                  .querySelectorAll('#conductor-form .see-more')
                  .forEach(function (element) {
                    // Remove any existing 'click' event listeners to prevent duplicate handlers.
                    element.removeEventListener('click', seeMoreLess);
                    // Add the new 'click' event listener.
                    element.addEventListener('click', seeMoreLess);
                  });

                // Find all '.see-less' buttons within '#conductor-form' and add 'click' event listeners.
                document
                  .querySelectorAll('#conductor-form .see-less')
                  .forEach(function (element) {
                    // Remove any existing 'click' event listeners to prevent duplicate handlers.
                    element.removeEventListener('click', seeMoreLess);
                    // Add the new 'click' event listener.
                    element.addEventListener('click', seeMoreLess);
                  });
              }
            });
          });
        });

        observer.observe($('#conductor-form')[0], {
          childList: 'TRUE',
          subtree: 'TRUE',
        });
      }
    },
  };
})(jQuery, Drupal);
