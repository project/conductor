<?php

namespace Drupal\conductor\Form;

use Drupal\conductor\Exception\ConductorApiRequestException;
use Drupal\conductor\Service\APIConnector\ConductorAPIService;
use Drupal\conductor\Service\ConductorHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Render\Markup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Module settings form.
 *
 * @codeCoverageIgnore
 */
class ConductorConfigForm extends ConfigFormBase {

  /**
   * Conductor Helper.
   *
   * @var \Drupal\conductor\Service\ConductorHelper
   */
  protected $conductorHelper;

  /**
   * Messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * Conductor API Service.
   *
   * @var \Drupal\conductor\Service\APIConnector\ConductorAPIService
   */
  private ConductorAPIService $conductorAPIService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('conductor.helper'),
      $container->get('conductor.api_service'),
      $container->get('messenger'),
      );
  }

  /**
   * Constructs class variables.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\conductor\Service\ConductorHelper $conductor_helper
   *   Conductor Helper service.
   * @param \Drupal\conductor\Service\APIConnector\ConductorAPIService $conductorAPIService
   *   Conductor Helper service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   Drupal messenger.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typedConfigManager, ConductorHelper $conductor_helper, ConductorAPIService $conductorAPIService, Messenger $messenger) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->conductorHelper = $conductor_helper;
    $this->conductorAPIService = $conductorAPIService;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'conductor_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Default settings.
    $config = $this->config('conductor.settings');

    // API key.
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#required' => TRUE,
      '#default_value' => !empty($config->get('conductor.api_key')) ? $config->get('conductor.api_key') : '',
      '#description' => $this->t('Get an API key by subscribing and generating one on https://app.conductor.com/u/24936/api-key/'),
    ];

    // Shared secret.
    $form['shared_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Shared Secret'),
      '#required' => TRUE,
      '#default_value' => !empty($config->get('conductor.shared_secret')) ? $config->get('conductor.shared_secret') : '',
      '#description' => $this->t('Get an API key by subscribing and generating one on https://app.conductor.com/u/24936/api-key/'),
    ];

    // Content Types.
    $form['content_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Included Content Types'),
      '#options' => $this->conductorHelper->getContentTypes(),
      '#default_value' => !empty($config->get('conductor.content_types')) ? $config->get('conductor.content_types') : [],
    ];

    $rank_sources = $this->conductorAPIService->getRankSources();
    // Rank Sources.
    $form['rank_sources'] = [
      '#type' => 'select',
      '#title' => $this->t('Rank Sources'),
      '#options' => $rank_sources,
      '#access' => !empty($rank_sources),
      '#default_value' => !empty($config->get('conductor.rank_sources')) ? $config->get('conductor.rank_sources') : 1,
    ];

    // Verbose Logging.
    $form['logging'] = [
      '#type' => 'radios',
      '#title' => $this->t('Verbose Logging'),
      '#options' => [t('Disable'), t('Enable')],
      '#default_value' => !empty($config->get('conductor.logging')) ? $config->get('conductor.logging') : '0',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('conductor.settings');
    $config->set('conductor.api_key', $form_state->getValue('api_key'));
    $config->set('conductor.shared_secret', $form_state->getValue('shared_secret'));
    $config->set('conductor.content_types', $form_state->getValue('content_types'));
    $config->set('conductor.rank_sources', $form_state->getValue('rank_sources'));
    $config->set('conductor.logging', $form_state->getValue('logging'));
    $config->save();

    try {
      $account_id = $this->conductorAPIService->getAccountId();
      // Check account id if received.
      if (!empty($account_id)) {
        $this->messenger->addMessage(Markup::create('Successfully established connection to Conductor API.'));
      }
      else {
        $this->messenger
          ->addError(Markup::create('Unable to connect to Conductor API. Please check credentials or <a href="https://app.conductor.com/u/password/forgot" target="_blank">reset your password</a>'));
      }

      return parent::submitForm($form, $form_state);

    }
    catch (ConductorApiRequestException $e) {
      $this->messenger->addError(Markup::create('Unable to connect to Conductor API. Please contact site administrator.'));
    }
    catch (\throwable $e) {
      $this->messenger->addError(Markup::create('Something went wrong. Please contact site administrator.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'conductor.settings',
    ];
  }

}
