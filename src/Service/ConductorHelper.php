<?php

declare(strict_types=1);

namespace Drupal\conductor\Service;

use Drupal\conductor\Service\APIConnector\ConductorConnector;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Theme\ThemeInitializationInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\node\NodeInterface;
use Psr\Log\LoggerInterface;

/**
 * Provides Conductor API helper services.
 *
 * @package Drupal\conductor\Service\ConductorHelper
 */
class ConductorHelper {

  use StringTranslationTrait;

  /**
   * Conductor Helper.
   *
   * @var \Drupal\conductor\Service\APIConnector\ConductorConnector
   */
  protected $conductorConnector;

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Module Extension List.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleBasePath;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Theme Initialization.
   *
   * @var \Drupal\Core\Theme\ThemeInitializationInterface
   */
  protected ThemeInitializationInterface $theme;

  /**
   * Theme Manager.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected ThemeManagerInterface $themeManager;

  /**
   * Constructs class variables.
   *
   * @param \Drupal\conductor\Service\APIConnector\ConductorConnector $conductor_connector
   *   Conductor Connector service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_extension
   *   Module Extension List.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_service
   *   The configuration service.
   * @param \Psr\Log\LoggerInterface $logger
   *   Drupal Logger Factory.
   */
  public function __construct(ConductorConnector $conductor_connector, EntityTypeManagerInterface $entity_type_manager, ModuleExtensionList $module_extension, RendererInterface $renderer, ConfigFactoryInterface $config_service, LoggerInterface $logger) {
    $this->conductorConnector = $conductor_connector;
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleBasePath = $module_extension->getPath('conductor');
    $this->renderer = $renderer;
    $this->config = $config_service;
    $this->logger = $logger;
  }

  /**
   * Check content type validation.
   *
   * @param object $form_state
   *   The current state of the form.
   * @param string $form_id
   *   A string that is the unique ID of the form.
   */
  public function checkContentTypeValidation($form_state, $form_id): bool {
    if (substr($form_id, 0, 5) !== 'node_' || substr($form_id, -5) !== '_form') {
      // Not a node form `node_..._form`.
      return FALSE;
    }

    if ($form_state->getFormObject() instanceof EntityFormInterface) {
      /** @var \Drupal\node\NodeInterface $node */
      $node = $form_state->getFormObject()->getEntity();
      if (!$node instanceof NodeInterface) {
        // Ensure we have the Node entity to proceed.
        return FALSE;
      }

      $bundle = $node->bundle();
      $config = $this->config->get('conductor.settings');
      $content_types = !empty($config->get('conductor.content_types')) ? $config->get('conductor.content_types') : [];
      if (!in_array($bundle, $content_types)) {
        return FALSE;
      }

      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get list of content types.
   */
  public function getContentTypes() {
    $types = [];
    $content_types = $this->entityTypeManager
      ->getStorage('node_type')
      ->loadMultiple();

    foreach ($content_types as $name => $type) {
      $types[$name] = !empty($type->get('name')) ? $type->get('name') : '';
    }

    return $types;
  }

  /**
   * Format number from 1000 to 1k.
   *
   * @param int $number
   *   Number to convert.
   */
  public function convertNumberToK($number) {
    if ($number >= 1000000) {
      return $number / 1000000 . " M";
    }
    elseif ($number >= 1000) {
      return $number / 1000 . " K";
    }
    else {
      return $number;
    }
  }

  /**
   * Convert decimal to percent.
   *
   * @param int $number
   *   Number to convert.
   */
  public function convertToPercent($number) {
    return floor((float) $number * 100) . '%';
  }

  /**
   * Error message.
   */
  public function errorMessage() {
    return $this->t('No Data Found.');
  }

}
