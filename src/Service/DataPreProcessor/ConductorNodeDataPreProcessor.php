<?php

declare(strict_types=1);

namespace Drupal\conductor\Service\DataPreProcessor;

use Drupal\Component\Utility\DeprecationHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Theme\ThemeInitializationInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\node\Entity\Node;

/**
 * Pre process node form data.
 */
class ConductorNodeDataPreProcessor {

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private ConfigFactoryInterface $config;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * Renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  private RendererInterface $renderer;

  /**
   * Theme initialization service.
   *
   * @var \Drupal\Core\Theme\ThemeInitializationInterface
   */
  private ThemeInitializationInterface $theme;

  /**
   * Theme manager service.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  private ThemeManagerInterface $themeManager;

  public function __construct(ConfigFactoryInterface $configFactory, EntityTypeManagerInterface $entityTypeManager, RendererInterface $renderer, ThemeInitializationInterface $theme, ThemeManagerInterface $theme_manager) {
    $this->config = $configFactory;
    $this->entityTypeManager = $entityTypeManager;
    $this->renderer = $renderer;
    $this->theme = $theme;
    $this->themeManager = $theme_manager;
  }

  /**
   * Generates the body copy from the $form_state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @throws \Drupal\Core\Theme\MissingThemeDependencyException
   */
  public function getBodyCopy(FormStateInterface $form_state) {
    $nid = $form_state->getValue(['conductor', 'nid']) ?? '';
    $node = $form_state->getFormObject()->getEntity();

    if (empty($nid)) {
      $node->in_preview = TRUE;
    }

    // Build body copy.
    return $this->buildPreview($node);
  }

  /**
   * Build body copy.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The Node entity.
   *
   * @throws \Drupal\Core\Theme\MissingThemeDependencyException
   */
  public function buildPreview(Node $node) {

    // Set the default theme as the current active theme.
    $default_theme = $this->config->get('system.theme')->get('default');
    $default_theme_object = $this->theme->getActiveThemeByName($default_theme);
    $this->themeManager->setActiveTheme($default_theme_object);

    $view_builder = $this->entityTypeManager->getViewBuilder('node');
    $content = $view_builder->view($node, 'full');
    $content['#conductor_uuid'] = $node->uuid();
    $build = [
      '#type' => 'html',
      'page' => [
        '#theme' => 'page',
        'content' => [
          'page_title' => [
            '#type' => 'page_title',
            '#title' => $node->getTitle(),
          ],
          'main_content' => [
            'content' => $content,
          ],
        ],
      ],
    ];

    $value = class_exists(DeprecationHelper::class) ? DeprecationHelper::backwardsCompatibleCall(
      \Drupal::VERSION,
      "10.3",
      fn() => $this->renderer->renderInIsolation($build),
      fn() => $this->renderer->renderPlain($build),
    // @phpstan-ignore-next-line
    ) : $this->renderer->renderPlain($build);

    // Reset theme to the admin theme.
    $this->themeManager->resetActiveTheme();

    // Remove script tags from string and return.
    return $this->removeScriptTags($value);
  }

  /**
   * Removes script tags from string.
   */
  public function removeScriptTags($value): string {
    return preg_replace('#&lt;script(.*?)&gt;(.*?)&lt;/script&gt;|<script(.*?)>(.*?)</script>#is', '', (string) $value);
  }

}
