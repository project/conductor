<?php

declare(strict_types=1);

namespace Drupal\conductor\Service;

use Drupal\conductor\Exception\ConductorApiRequestException;
use Drupal\conductor\Service\Accordion\ConductorAccordionDataMapper;
use Drupal\conductor\Service\APIConnector\ConductorAPIService;
use Drupal\conductor\Service\DataPreProcessor\ConductorNodeDataPreProcessor;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Psr\Log\LoggerInterface;

/**
 * Manages Conductor recommendations requests.
 */
class ConductorManager {

  use StringTranslationTrait;

  /**
   * Helper service.
   *
   * @var \Drupal\conductor\Service\ConductorHelper
   */
  private ConductorHelper $conductorHelper;

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private ConfigFactoryInterface $configFactory;

  /**
   * Log interface.
   *
   * @var \Psr\Log\LoggerInterface
   */
  private LoggerInterface $logger;

  /**
   * Data mapper service.
   *
   * @var \Drupal\conductor\Service\Accordion\ConductorAccordionDataMapper
   */
  private ConductorAccordionDataMapper $conductorAccordionDataMapper;

  /**
   * Node data pre processor service.
   *
   * @var \Drupal\conductor\Service\DataPreProcessor\ConductorNodeDataPreProcessor
   */
  private ConductorNodeDataPreProcessor $conductorNodeDataProcessor;

  /**
   * Conductor API Service.
   *
   * @var \Drupal\conductor\Service\APIConnector\ConductorAPIService
   */
  private ConductorAPIService $conductorAPIService;

  public function __construct(ConductorAccordionDataMapper $conductor_accordion_data_mapper, ConductorAPIService $conductorAPIService, ConfigFactoryInterface $config_factory, ConductorNodeDataPreProcessor $conductorNodeDataProcessor, LoggerInterface $logger) {
    $this->conductorAccordionDataMapper = $conductor_accordion_data_mapper;
    $this->conductorAPIService = $conductorAPIService;
    $this->configFactory = $config_factory;
    $this->conductorNodeDataProcessor = $conductorNodeDataProcessor;
    $this->logger = $logger;
  }

  /**
   * Builds Accordion.
   */
  public function buildAccordion(array $form, FormStateInterface $form_state, AjaxResponse $response): AjaxResponse {
    try {
      // Default settings.
      $config = $this->configFactory->get('conductor.settings');
      // Get account id.
      $account_id = $this->conductorAPIService->getAccountId();
      // Get web property id.
      $web_property_id = $this->conductorAPIService->getWebPropertyId($account_id);
      // Get topic inserted by user.
      $safe_topic_value = is_null($form_state->getValue(['conductor', 'keyword_container', 'topic']))
          ? "" : urlencode($form_state->getValue(['conductor', 'keyword_container', 'topic']));
      // Check if request_id is already set, if not, get it.
      $request_id_default = !empty($form_state->getValue(['conductor', 'request_id']))
          ? json_decode($form_state->getValue(['conductor', 'request_id']), TRUE) : [];
      if (!empty($request_id_default[$safe_topic_value])) {
        $request_id = $request_id_default[$safe_topic_value]['id'];
      }
      else {
        // Get body copy.
        $body_copy = $this->conductorNodeDataProcessor->getBodyCopy($form_state);
        // Get request id.
        $request_id = $this->conductorAPIService->getRequestId($account_id, $web_property_id, $config, $body_copy, $form_state);
        // Set request_id value.
        $request_array = [
          $safe_topic_value => [
            'id' => $request_id,
            'body' => $body_copy,
          ],
        ];
        $response->addCommand(new InvokeCommand('#request_id', 'attr', ['value', json_encode($request_array)]));
      }
      // Fetch recommendations.
      $data = $this->conductorAPIService->fetchRecommendations(
        $account_id,
        $request_id,
      );
      // Map data.
      $response = $this->conductorAccordionDataMapper->mapData($response, $data, $request_id);
    }
    catch (ConductorApiRequestException $e) {
      $response->addCommand(
        new HtmlCommand(
          '.conductor__message',
          '<div class="conductor__error request__exception"><p><b>' . $this->t('Could not connect to Conductor API at this time.') . '</b> ' . $this->t('Please try again later.') . '</p></div>',
        )
      );

      $response->addCommand(
        new InvokeCommand('.conductor__preloader', 'removeClass', ['generating']),
      );

      $response->addCommand(
        new InvokeCommand('.conductor__preloader', 'addClass', ['generated']),
      );

      $response->addCommand(
        new InvokeCommand('#conductor-form .conductor__keyword-container input[type="submit"]', 'removeAttr', ['disabled']),
      );

      $response->addCommand(
        new InvokeCommand('#conductor-form .conductor__keyword-container input[type="submit"]', 'removeClass', ['generating']),
      );

      return $response;

    }
    catch (\Throwable $e) {

      $this->logger->error($e->getMessage());

      $response->addCommand(
        new HtmlCommand(
          '.conductor__message',
          '<div class="conductor__error"><p><b>' . $this->t('Could not generate guidance at this time.') . '</b> ' . $this->t('Please try again later.') . '</p></div>',
        )
      );

      $response->addCommand(
        new InvokeCommand('.conductor__preloader', 'removeClass', ['generating']),
      );

      $response->addCommand(
        new InvokeCommand('.conductor__preloader', 'addClass', ['generated']),
      );

      $response->addCommand(
        new InvokeCommand('#conductor-form .conductor__keyword-container input[type="submit"]', 'removeAttr', ['disabled']),
      );

      $response->addCommand(
        new InvokeCommand('#conductor-form .conductor__keyword-container input[type="submit"]', 'removeClass', ['generating']),
      );

      return $response;
    }

    return $response;

  }

}
