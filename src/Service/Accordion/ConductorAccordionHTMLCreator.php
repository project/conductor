<?php

declare(strict_types=1);

namespace Drupal\conductor\Service\Accordion;

use Drupal\conductor\Service\ConductorHelper;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Psr\Log\LoggerInterface;

/**
 * Conductor Accordion HTML Creator.
 */
class ConductorAccordionHTMLCreator {

  /* String translation trait. */
  use StringTranslationTrait;

  /**
   * Constructs a new ConductorAccordionHTMLCreator object.
   *
   * @var \Drupal\conductor\Service\ConductorHelper
   */
  private ConductorHelper $conductorHelper;

  /**
   * Constructs a renderer object.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  private RendererInterface $renderer;

  /**
   * Constructs a logger object.
   *
   * @var \Psr\Log\LoggerInterface
   */
  private LoggerInterface $logger;

  /**
   * Module base path.
   *
   * @var string
   */
  protected $moduleBasePath;

  /**
   * The constructor.
   *
   * @param \Drupal\conductor\Service\ConductorHelper $conductorHelper
   *   The conductor helper service.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger interface.
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_extension
   *   The module extension list.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer interface.
   */
  public function __construct(ConductorHelper $conductorHelper, LoggerInterface $logger, ModuleExtensionList $module_extension, RendererInterface $renderer) {
    $this->conductorHelper = $conductorHelper;
    $this->logger = $logger;
    $this->moduleBasePath = $module_extension->getPath('conductor');
    $this->renderer = $renderer;
  }

  /**
   * Construct guidance status class.
   *
   * @param array $data
   *   Endpoints data array.
   */
  public function constructGuidanceStatusClass($data) {
    $status_mapping = [
      $data['status'] ?? '',
      $data['insightsStatus'] ?? '',
      $data['snippetsStatus'] ?? '',
    ];
    $guidance_status = '';
    if (in_array('PROCESSING', $status_mapping)) {
      $guidance_status = 'processing';
    }
    elseif (in_array('COMPLETED_WITH_ERROR', $status_mapping) || in_array('FAILED', $status_mapping)) {
      $guidance_status = 'failed';
    }
    elseif (in_array('COMPLETED', $status_mapping)) {
      $guidance_status = 'completed';
    }

    return $guidance_status;
  }

  /**
   * Construct keyword insights table row.
   *
   * @param string $type
   *   Result type.
   * @param string $summary
   *   Result summary.
   * @param array $keywords
   *   Keyword array.
   * @param string $request_id
   *   Request id.
   *
   * @throws \Exception
   */
  public function constructKeywordsInsight($type, $summary, $keywords, $request_id) {
    $label = '';
    $build = [
      '#theme' => 'table',
      '#header' => [
        [
          'data' => $this->t('Keyword'),
          'scope' => 'col',
        ],
        [
          'data' => $this->t('Volume'),
          'scope' => 'col',
        ],
        [
          'data' => $this->t('Your draft'),
          'scope' => 'col',
        ],
      ],
    ];
    if ($type === 'PRIMARY_KEYWORDS_INSIGHT') {
      $label = $this->t('Primary keywords to target');
    }
    elseif ($type === 'SECONDARY_KEYWORDS_INSIGHT') {
      $label = $this->t('Secondary keywords to target');
    }
    // Construct Keywords.
    $keywords = $keywords['details']['keywords']['items'] ?? [];
    $i = 0;
    foreach ($keywords as $keyword) {
      $draft = !empty($keyword['ownRank']) ? $this->t('Yes') : $this->t('No');
      $volume = !empty($keyword['msv']) ? $this->conductorHelper->convertNumberToK($keyword['msv']) : '';
      $class = ($i > 4) ? 'hidden-row hidden' : '';
      $build['#rows'][] = [
        'data' => [
          [
            'data' => $keyword['keyword'],
          ],
          [
            'data' => $volume,
          ],
          [
            'data' => $draft,
          ],
        ],
        'class' => $class,
      ];
      $i++;
    }
    // Append see more/less if more than 5.
    if ($i > 5) {
      $build['#rows'][] = $this->seeMore();
      $build['#rows'][] = $this->seeLess();
    }

    // Error message.
    if ($i === 0) {
      $this->logger->warning('No keywords found for Request ID:' . $request_id);
      $build['#rows'][] = [
        'data' => [
          [
            'data' => ['#markup' => $this->conductorHelper->errorMessage()],
          ],
        ],
      ];
      $build['#header'] = [];
    }
    $build = $this->renderer->render($build);

    $html['content'] = [
      '#theme' => 'conductor_keyword_insights',
      '#label' => $label,
      '#summary' => $summary,
      '#build' => $build,
    ];

    return $this->renderer->render($html);
  }

  /**
   * Construct pattern insights table row.
   *
   * @param string $type
   *   Result type.
   * @param string $summary
   *   Result summary.
   * @param array $keywords
   *   Keyword array.
   * @param string $request_id
   *   Request id.
   *
   * @throws \Exception
   */
  public function constructPatternsInsight($type, $summary, $keywords, $request_id) {
    $header_mapping = [
      'HTML_HEADING1_COMMON' => [
        'heading' => 'H1',
        'label' => $this->t('H1 Patterns'),
      ],
      'HTML_HEADING2_COMMON' => [
        'heading' => 'H2',
        'label' => $this->t('H2 Patterns'),
      ],
      'HTML_HEADING3_COMMON' => [
        'heading' => 'H3',
        'label' => $this->t('H3 Patterns'),
      ],
      'HTML_TITLE_COMMON' => [
        'heading' => 'title',
        'label' => $this->t('Title tag patterns'),
      ],
      'HTML_META_DESCRIPTION_COMMON' => [
        'heading' => 'description',
        'label' => $this->t('Meta description patterns'),
      ],
    ];
    $heading = $this->t('Your') . ' ' . $header_mapping[$type]['heading'];
    $build = [
      '#theme' => 'table',
      '#header' => [
        [
          'data' => $this->t('Keyword'),
          'scope' => 'col',
        ],
        [
          'data' => $this->t('Ranking Pages'),
          'scope' => 'col',
        ],
        [
          'data' => $heading,
          'scope' => 'col',
        ],
      ],
    ];
    // Construct Keywords.
    $total = $keywords['details']['keywords']['numOfSerps'] ?? '';
    $keywords = $keywords['details']['keywords']['items'] ?? [];
    $i = 0;
    foreach ($keywords as $keyword) {
      $your_pattern = $keyword['ownMatched'] ? $this->t('Yes') : $this->t('No');
      $class = ($i > 4) ? 'hidden-row hidden' : '';
      $build['#rows'][] = [
        'data' => [
          [
            'data' => $keyword['keyword'],
          ],
          [
            'data' => $keyword['numOfOccurrence'] . '/' . $total,
          ],
          [
            'data' => $your_pattern,
          ],
        ],
        'class' => $class,
      ];
      $i++;
    }
    // Append see more/less if more than 5.
    if ($i > 4) {
      $build['#rows'][] = $this->seeMore();
      $build['#rows'][] = $this->seeLess();
    }
    // Error message.
    if ($i == 0) {
      $this->logger->warning('No patterns found for  Request ID:' . $request_id);
      $build['#rows'][] = [
        'data' => [
          [
            'data' => ['#markup' => $this->conductorHelper->errorMessage()],
          ],
        ],
      ];
      $build['#header'] = [];
    }
    $build = $this->renderer->render($build);

    $html['content'] = [
      '#theme' => 'conductor_patterns_insights',
      '#label' => $header_mapping[$type]['label'],
      '#summary' => str_replace(':.', ':', $summary),
      '#build' => $build,
    ];

    return $this->renderer->render($html);
  }

  /**
   * Construct Content Optimization Body Copy table row.
   *
   * @param array $keywords
   *   Keywords array.
   * @param string $request_id
   *   Request id.
   *
   * @throws \Exception
   */
  public function constructContentOptimizationBodyCopy($keywords, $request_id) {
    $presets = [
      'IMPORTANT',
      'SUPPORTING',
      'ADDITIONAL',
    ];
    // Construct Keywords.
    $total = $keywords['details']['keywords']['maxOccurrence'] ?? '';
    $factors = $keywords['details']['predictiveFactors'] ?? [];
    $keywords = $keywords['details']['keywords']['items'] ?? [];
    $tables = [];
    // Construct Summary.
    $summary = [];
    foreach ($factors as $factor) {
      $summary[] = $factor['message'];
    }
    $summary = implode(' ', $summary);
    // Preset grouping.
    $body_copy_array = [];
    foreach ($presets as $preset) {
      $body_copy_array[$preset] = array_filter($keywords, function ($keyword) use ($preset) {
        return array_values($keyword['presets'])[0] === $preset;
      });
    }
    // Keyword rows.
    foreach ($body_copy_array as $preset_key => $keywords_data) {
      $build = [
        '#theme' => 'table',
        '#attributes' => ['class' => 'conductor__body-copy'],
      ];
      // Help text message.
      switch ($preset_key) {
        case 'IMPORTANT':
          $label = $this->t('Important Keywords');
          $text = $this->t('Most of the top 15 ranking pages for your target topic use these semantically relevant terms.');
          break;

        case 'SUPPORTING':
          $label = $this->t('Supporting Keywords');
          $text = $this->t('Many (but not all) of your SEO competitors use these terms in their pages ranking for your target topic. They are semantically relevant, but not as relevant as the Important keywords.');
          break;

        case 'ADDITIONAL':
          $label = $this->t('Additional Keywords');
          $text = $this->t('Some of your SEO competitors use these terms in their pages ranking for your target topic. They are semantically associated with your target topic, but they may not be directly relevant.');
          break;

        default:
          $label = '';
          $text = '';
      }
      $help_text = '<p><b>' . $label . ':</b></p><p>' . $text . '</p>';
      // Preset header mapping.
      $header_html = '
        <h5>' . ucfirst(strtolower($preset_key)) . '</h5>
        <img src="/' . $this->moduleBasePath . '/images/help-circle.svg" />
        <div class="conductor__help-text__popup">
          <div class="conductor__help-text__popup__text">' . $help_text . '</div>
        </div>
      ';
      $build['#rows'][] = [
        'data' => [
          [
            'data' => ['#markup' => $header_html],
            'colspan' => 3,
            'class' => 'conductor__help-text__container',
          ],
        ],
      ];
      $build['#rows'][] = [
        'data' => [
          [
            'data' => $this->t('Term'),
          ],
          [
            'data' => $this->t('Ranking pages'),
          ],
          [
            'data' => $this->t('Your draft'),
          ],
        ],
        'class' => 'conductor__body-copy__header',
      ];
      // Inner table rows.
      $i = 0;
      foreach ($keywords_data as $keyword) {
        $draft = !empty($keyword['ownFrequency']) ? $this->t('Yes') : $this->t('No');
        $class = ($i > 4) ? 'hidden-row hidden' : '';
        $build['#rows'][] = [
          'data' => [
            [
              'data' => $keyword['keyword'],
            ],
            [
              'data' => $keyword['numOfOccurrence'] . '/' . $total,
            ],
            [
              'data' => $draft,
            ],
          ],
          'class' => $class,
        ];
        $i++;
      }
      // Append see more/less if more than 5.
      if ($i > 4) {
        $build['#rows'][] = $this->seeMore();
        $build['#rows'][] = $this->seeLess();
      }
      // Error message.
      if ($i == 0) {
        $this->logger->warning('No body copy suggestions found for  Request ID:' . $request_id);
        $build['#rows'][] = [
          'data' => [
            [
              'data' => ['#markup' => $this->conductorHelper->errorMessage()],
            ],
          ],
        ];
      }
      $tables[] = $this->renderer->render($build);
    }

    $html['content'] = [
      '#theme' => 'conductor_body_copy',
      '#summary' => $summary,
      '#build' => implode('', $tables),
    ];

    return $this->renderer->render($html);
  }

  /**
   * Construct Content Score.
   *
   * @param array $details
   *   Snippet details array.
   *
   * @throws \Exception
   */
  public function constructContentScore($details) {
    $html = ['#markup' => ''];
    if (!empty($details['content_score'])) {
      $percentage = $this->conductorHelper->convertToPercent($details['content_score']);
      $html['content'] = [
        '#theme' => 'conductor_content_score',
        '#percentage' => $percentage,
        '#module_base_path' => $this->moduleBasePath,
      ];
    }

    return $this->renderer->render($html);
  }

  /**
   * Construct Score Factors.
   *
   * @param array $keywords
   *   Keywords array.
   * @param string $request_id
   *   Request id.
   *
   * @throws \Exception
   */
  public function constructScoreFactors($keywords, $request_id) {
    $error_message = NULL;
    // Detail Mapping.
    $keyword = $this->t('Keyword coverage');
    $sentence = $this->t('Sentence structure');
    $repetition = $this->t('Repetitions');

    $coverage_score = 'Not found';
    if (array_key_exists('coverage_score', $keywords)) {
      $coverage_score = $this->conductorHelper->convertToPercent($keywords['coverage_score']);
    }

    $natural_language_score = 'Not found';
    if (array_key_exists('natural_language_score', $keywords)) {
      $natural_language_score = $this->conductorHelper->convertToPercent($keywords['natural_language_score']);
    }

    $repetition_score = 'Not found';
    if (array_key_exists('repetition_score', $keywords)) {
      $repetition_score = $this->conductorHelper->convertToPercent($keywords['repetition_score']);
    }

    $detail_mapping = [
      'coverage_score' => [
        'label' => '<p>' . $keyword . ': <b>' . $coverage_score . '</b></p>',
        'help_text' => '<p>' . $this->t('Calculates your keyword coverage, based on our Body Copy Suggestions. The more recommended keywords you add in your text, the higher the score.') . '</p>',
      ],
      'natural_language_score' => [
        'label' => '<p>' . $sentence . ': <b>' . $natural_language_score . '</b></p>',
        'help_text' => '<p>' . $this->t('Assesses how well your text is structured. Our deep learning algorithm detects when your content does not use natural language, and calculates the score accordingly. For example, if you write a phrase like "I eats fish", the algorithm will detect the incorrect usage and lower your score.') . '</p>',
      ],
      'repetition_score' => [
        'label' => '<p>' . $repetition . ': <b>' . $repetition_score . '</b></p>',
        'help_text' => '<p>' . $this->t('Assesses how often you repeat text in your content. If you are heavily repeating sentences and phrases, your score will be negatively impacted. This does not apply to individual words—only phrases and sentences.') . '</p>',
      ],
    ];

    if (empty($keywords['repetition_score']) && empty($keywords['natural_language_score']) && empty($keywords['coverage_score'])) {
      $error_message = $this->conductorHelper->errorMessage();
      $this->logger->warning('Conductor API: Score factors are empty for request ID:' . $request_id);
    }

    $html['content'] = [
      '#theme' => 'conductor_score_factors',
      '#detail_mapping' => $detail_mapping,
      '#module_base_path' => $this->moduleBasePath,
      '#error_message' => $error_message,
    ];

    return $this->renderer->render($html);
  }

  /**
   * Construct Questions to answer.
   *
   * @param array $keywords
   *   Keywords array.
   * @param string $request_id
   *   Request id.
   *
   * @throws \Exception
   */
  public function constructQuestions($keywords, $request_id) {
    $build = [
      '#theme' => 'table',
    ];
    $items = $keywords['details']['paaQuestions']['items'] ?? [];
    if (!empty($items)) {
      $i = 0;
      foreach ($items as $item) {
        $question = $item['text'] ?? '';
        $answer = $item['answer'] ?? '';
        $class = ($i > 1) ? 'hidden-row hidden' : '';
        $answer = !empty($answer) ? '<p class="conductor__accordion-content__summary"><i>' . $answer . '</i></p>' : '';
        $build['#rows'][] = [
          'data' => [
            [
              'data' => [
                '#markup' => '<h4 class="conductor__accordion-content__label">' . $question . '</h4>' . $answer,
              ],
              'colspan' => 3,
            ],
          ],
          'class' => $class,
        ];
        $i++;
      }
      // Append see more/less if more than 2.
      if ($i > 1) {
        $build['#rows'][] = $this->seeMore();
        $build['#rows'][] = $this->seeLesS();
      }
    }

    // Set error message.
    if (empty($build['#rows'])) {
      $this->logger->warning('Conductor API: Questions are empty for request ID:' . $request_id);
      $build['#rows'][] = [
        'data' => [
          [
            'data' => ['#markup' => $this->conductorHelper->errorMessage()],
          ],
        ],
      ];
    }
    $build = $this->renderer->render($build);

    $html['content'] = [
      '#theme' => 'conductor_questions_to_answer',
      '#build' => $build,
    ];

    return $this->renderer->render($html);
  }

  /**
   * Construct Content Length.
   *
   * @param array $keywords
   *   Keywords array.
   * @param string $request_id
   *   Request id.
   *
   * @throws \Exception
   */
  public function constructContentLength($keywords, $request_id) {
    $error_message = NULL;
    $details = $keywords['details'] ?? [];
    $factors = $details['predictiveFactors'] ?? [];
    $metric_value = !empty($details['own']['metricValue']) ? number_format($details['own']['metricValue']) : 0;
    $average_metric_value = !empty($details['serp']['averageMetricValue']) ? number_format($details['serp']['averageMetricValue']) : 0;
    // Construct Summary.
    $summary = [];
    foreach ($factors as $factor) {
      $summary[] = $factor['message'];
    }
    $summary = implode(' ', $summary);

    if (empty($average_metric_value)) {
      $error_message = $this->conductorHelper->errorMessage();
      $this->logger->warning('Conductor API: Content Length is empty for request ID:' . $request_id);
    }

    $html['content'] = [
      '#theme' => 'conductor_content_length',
      '#summary' => $summary,
      '#metric_value' => $metric_value . " words",
      '#average_metric_value' => $average_metric_value . " words",
      '#error_message' => $error_message,
    ];

    return $this->renderer->render($html);
  }

  /**
   * Construct Readability Insight.
   *
   * @param string $summary
   *   Result summary.
   * @param array $keywords
   *   Keywords array.
   * @param string $request_id
   *   Request id.
   *
   * @throws \Exception
   */
  public function constructReadabilityInsight($summary, $keywords, $request_id) {
    $error_message = NULL;
    $details = $keywords['details'] ?? [];
    $your_draft = !empty($details['own']['metricValue']['label']) ? $details['own']['metricValue']['label'] : 'N/A';
    $average = $details['serp']['averageMetricValue']['label'] ?? [];

    if (empty($average)) {
      $error_message = $this->conductorHelper->errorMessage();
      $this->logger->warning('Conductor API: Readability is empty for request ID:' . $request_id);
    }

    $html['content'] = [
      '#theme' => 'conductor_readability_insights',
      '#summary' => $summary,
      '#your_draft' => $your_draft,
      '#average' => $average,
      '#error_message' => $error_message,
    ];

    return $this->renderer->render($html);
  }

  /**
   * See more Form API.
   */
  private function seeMore() {
    return [
      'data' => [
        [
          'data' => [
            '#markup' => '<a href="#" class="see-more text-center">' . $this->t('See more') . '</a>',
          ],
          'colspan' => 3,
        ],
      ],
      '#attributes' => ['class' => 'see-wrapper'],
    ];
  }

  /**
   * See less Form API.
   */
  private function seeLess() {
    return [
      'data' => [
        [
          'data' => [
            '#markup' => '<a href="#" class="see-less hidden text-center">' . $this->t('See Less') . '</a>',
          ],
          'colspan' => 3,
        ],
      ],
      '#attributes' => ['class' => 'see-wrapper'],
    ];
  }

}
