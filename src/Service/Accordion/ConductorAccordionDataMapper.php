<?php

declare(strict_types=1);

namespace Drupal\conductor\Service\Accordion;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;

/**
 * The data mapper class.
 */
class ConductorAccordionDataMapper {

  /**
   * ConductorAccordionHTMLCreator service.
   *
   * @var \Drupal\conductor\Service\Accordion\ConductorAccordionHTMLCreator
   */
  private ConductorAccordionHTMLCreator $conductorAccordionHTMLCreator;

  public function __construct(ConductorAccordionHTMLCreator $conductorAccordionHTMLCreator) {
    $this->conductorAccordionHTMLCreator = $conductorAccordionHTMLCreator;
  }

  /**
   * Map data.
   *
   * @param \Drupal\Core\Ajax\AjaxResponse $response
   *   The response object.
   * @param array $data
   *   The data received from the API.
   * @param string $request_id
   *   The request id.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The response object.
   *
   * @throws \Exception
   */
  public function mapData(AjaxResponse $response, $data, string $request_id): AjaxResponse {

    // Data mapping.
    $target_topics_html = [];
    $patterns_html = [];
    $content_optimization_html = '';
    $content_score_html = '';
    $score_factors = '';
    $questions_html = '';
    $content_length_html = '';
    $readability_html = '';
    if (!empty($data['results'])) {
      // Initiate new request id on completed request.
      if (in_array($data['status'], ['COMPLETED', 'COMPLETED_WITH_ERROR', 'FAILED'])) {
        $response->addCommand(new InvokeCommand('#request_id', 'attr', ['value', '']));
      }
      // Insights.
      $insights = $data['results']['insights'] ?? [];
      foreach ($insights as $key => $insight) {
        $type = $insight['type'] ?? '';
        $summary = $insight['summary']['text'] ?? '';
        $keywords = $insight ?? [];
        // Target Topics.
        if ($type === 'PRIMARY_KEYWORDS_INSIGHT' || $type === 'SECONDARY_KEYWORDS_INSIGHT') {
          $keyword_sort = [
            'PRIMARY_KEYWORDS_INSIGHT' => 0,
            'SECONDARY_KEYWORDS_INSIGHT' => 1,
          ];
          $target_topics_html[$keyword_sort[$type]] = (string) ($this->conductorAccordionHTMLCreator->constructKeywordsInsight($type, $summary, $keywords, $request_id) ?? '');
        }
        // Patterns.
        if (in_array($type, [
          'HTML_HEADING1_COMMON',
          'HTML_HEADING2_COMMON',
          'HTML_HEADING3_COMMON',
          'HTML_TITLE_COMMON',
          'HTML_META_DESCRIPTION_COMMON',
        ])
        ) {
          $patterns_sort = [
            'HTML_HEADING1_COMMON' => 0,
            'HTML_HEADING2_COMMON' => 1,
            'HTML_HEADING3_COMMON' => 2,
            'HTML_TITLE_COMMON' => 3,
            'HTML_META_DESCRIPTION_COMMON' => 4,
          ];
          $patterns_html[$patterns_sort[$type]] = (string) ($this->conductorAccordionHTMLCreator->constructPatternsInsight($type, $summary, $keywords, $request_id) ?? '');
        }
        // Content Optimization.
        if ($type === 'CONTENT_ANALYSIS_INSIGHT') {
          $content_optimization_html .= $this->conductorAccordionHTMLCreator->constructContentOptimizationBodyCopy($keywords, $request_id) ?? '';
        }
        // Questions to answer.
        if ($type === 'QUESTIONS_INSIGHT_V2') {
          $questions_html .= $this->conductorAccordionHTMLCreator->constructQuestions($keywords, $request_id) ?? '';
        }
        // Content length.
        if ($type === 'CONTENT_LENGTH_INSIGHT') {
          $content_length_html .= $this->conductorAccordionHTMLCreator->constructContentLength($keywords, $request_id) ?? '';
        }
        // Readability insight.
        if ($type === 'READABILITY_INSIGHT') {
          $readability_html .= $this->conductorAccordionHTMLCreator->constructReadabilityInsight($summary, $keywords, $request_id) ?? '';
        }
      }

    }

    // Content Optimization set HTML.
    $response = $this->setContentOptimization($response, $score_factors, $content_optimization_html, $questions_html, $content_length_html, $readability_html, $request_id, $data, $content_score_html);

    // Target topics set HTML.
    $response = $this->setTargetTopics($response, $target_topics_html);

    // Patterns set HTML.
    $response = $this->setpatternsHtml($response, $patterns_html);

    // Set guidance status class.
    $response = $this->setGuidanceStatusClass($response, $data);

    // Set progress value.
    $response = $this->setProgressValue($response, $data);

    return $response;
  }

  /**
   * Set progress value.
   *
   * @param \Drupal\Core\Ajax\AjaxResponse $response
   *   The response object.
   * @param array $data
   *   The data received from the API.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The response object.
   */
  public function setProgressValue(AjaxResponse $response, array $data): AjaxResponse {
    // Reset.
    if (in_array($data['status'], ["COMPLETED", "FAILED", "COMPLETED_WITH_ERROR"]) || empty($data['progress'])) {
      $progress = '0%';
    }
    else {
      $progress = round(($data['progress']['completed'] / $data['progress']['total']) * 100) . "%";
    }

    $response->addCommand(
      new HtmlCommand(
        '.progress-container',
        '<div class="progress-bar" style=width:' . $progress . ';></div>'),
    );

    // Set steps message.
    $message = $this->getSteps($data);

    $response->addCommand(
      new HtmlCommand(
        '.guidance-steps',
        $message,
      ),
    );

    return $response;
  }

  /**
   * Get guidance generation steps.
   *
   * @param array $data
   *   The data received from the API.
   *
   * @return string
   *   The no. of steps completed.
   */
  public function getSteps($data): string {
    // Reset.
    if (in_array($data['status'], ["COMPLETED", "FAILED", "COMPLETED_WITH_ERROR"]) || empty($data['progress'])) {
      return "0 of 0 steps completed..";
    }

    return $data['progress']['completed'] . " of " . $data['progress']['total'] . " steps completed..";
  }

  /**
   * Set guidance status class.
   *
   * @param \Drupal\Core\Ajax\AjaxResponse $response
   *   The response object.
   * @param array $data
   *   The data received from the API.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The response object.
   */
  public function setGuidanceStatusClass(AjaxResponse $response, array $data): AjaxResponse {
    $guidance_status = $this->conductorAccordionHTMLCreator->constructGuidanceStatusClass($data);

    $response->addCommand(
      new HtmlCommand(
        '.conductor__message',
        '<div class="conductor__message-inner ' . $guidance_status . '"></div>'),
    );

    return $response;
  }

  /**
   * Set patterns HTML.
   *
   * @param \Drupal\Core\Ajax\AjaxResponse $response
   *   The response object.
   * @param array $patterns_html
   *   The patterns HTML.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The response object.
   */
  public function setPatternsHtml(AjaxResponse $response, array $patterns_html): AjaxResponse {
    $throbber = $this->getThrobber();
    ksort($patterns_html);
    $patterns_html = !empty($patterns_html) ? implode('', $patterns_html) : $throbber;

    $response->addCommand(
      new HtmlCommand(
        '.conductor__patterns .conductor__accordion-content',
        $patterns_html),
    );

    return $response;
  }

  /**
   * Set target topics HTML.
   *
   * @param \Drupal\Core\Ajax\AjaxResponse $response
   *   The response object.
   * @param array $target_topics_html
   *   The target topics HTML.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The response object.
   */
  public function setTargetTopics(AjaxResponse $response, array $target_topics_html): AjaxResponse {
    $throbber = $this->getThrobber();
    ksort($target_topics_html);
    $target_topics_html = !empty($target_topics_html) ? implode('', $target_topics_html) : $throbber;
    $response->addCommand(
      new HtmlCommand(
        '.conductor__target-topics .conductor__accordion-content',
        $target_topics_html),
    );

    return $response;
  }

  /**
   * Set content optimization HTML.
   *
   * @param \Drupal\Core\Ajax\AjaxResponse $response
   *   The response object.
   * @param string $score_factors
   *   The score factors.
   * @param string $content_optimization_html
   *   The content optimization HTML.
   * @param string $questions_html
   *   The questions HTML.
   * @param string $content_length_html
   *   The content length HTML.
   * @param string $readability_html
   *   The readability HTML.
   * @param string $request_id
   *   The request id.
   * @param array $data
   *   The data array.
   * @param string $content_score_html
   *   The content score html.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The response object.
   *
   * @throws \Exception
   */
  public function setContentOptimization(AjaxResponse $response, $score_factors, $content_optimization_html, $questions_html, $content_length_html, $readability_html, $request_id, $data, $content_score_html): AjaxResponse {
    $throbber = $this->getThrobber();
    $snippets = $data['results']['snippets'] ?? [];
    foreach ($snippets as $snippet) {
      $details = $snippet['details'];
      // Content score.
      $content_score_html .= $this->conductorAccordionHTMLCreator->constructContentScore($details);
      // Score factors.
      $score_factors .= $this->conductorAccordionHTMLCreator->constructScoreFactors($details, $request_id);

    }
    if (empty($content_score_html)
      && $data['status'] !== 'PROCESSING'
      && $data['insightsStatus'] !== 'PROCESSING'
      && $data['snippetsStatus'] !== 'PROCESSING'
    ) {
      $content_score_html = '<div class="conductor__error"><p class="conductor__content-score__error">Could not generate a Content Score. Please try again later.</p></div>';
    }
    if (!empty($content_score_html)) {
      $response->addCommand(
        new HtmlCommand(
          '.conductor__content-score',
          $content_score_html),
      );
    }

    if (!empty($score_factors)
      || !empty($content_optimization_html)
      || !empty($questions_html)
      || !empty($content_length_html)
      || !empty($readability_html)) {
      $throbber = '';
    }
    $response->addCommand(
      new HtmlCommand(
        '.conductor__content-optimization .conductor__accordion-content',
        $score_factors . $content_optimization_html . $questions_html . $content_length_html . $readability_html . $throbber),
    );

    return $response;
  }

  /**
   * Get throbber.
   */
  public function getThrobber(): string {
    return '<div class="conductor__accordion-content__preloader"><div class="ajax-progress__throbber">&nbsp;</div></div>';
  }

}
