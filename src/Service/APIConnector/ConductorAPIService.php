<?php

declare(strict_types=1);

namespace Drupal\conductor\Service\APIConnector;

use Drupal\conductor\Exception\ConductorApiRequestException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Psr\Log\LoggerInterface;

/**
 * Conductor API Service.
 */
class ConductorAPIService {

  /**
   * Conductor Connector service.
   *
   * @var \Drupal\conductor\Service\APIConnector\ConductorConnector
   */
  private ConductorConnector $conductorConnector;

  /**
   * Logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  private LoggerInterface $logger;

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private ConfigFactoryInterface $config;

  /**
   * The constructor.
   *
   * @param \Drupal\conductor\Service\APIConnector\ConductorConnector $conductor_connector
   *   The Conductor Connector service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger interface.
   */
  public function __construct(ConductorConnector $conductor_connector, ConfigFactoryInterface $config_factory, LoggerInterface $logger) {
    $this->conductorConnector = $conductor_connector;
    $this->config = $config_factory;
    $this->logger = $logger;
  }

  /**
   * Generate Recommendations.
   *
   * @param int $account_id
   *   Conductor Account ID.
   * @param string $request_id
   *   Content Guidance Request ID.
   * @param string $override_api_key
   *   Override for config API Key.
   * @param string $override_shared_secret
   *   Override for config Shared Secret.
   *
   * @throws \Exception
   */
  public function fetchRecommendations($account_id, $request_id, $override_api_key = '', $override_shared_secret = '') {
    $data = [];
    $url = '/' . $account_id . '/content-guidance?requestUuid=' . $request_id;
    if (!empty($request_id)) {
      // Get Recommendations.
      $data = $this->conductorConnector->request('GET', $url, [], $override_api_key, $override_shared_secret);
      // Log request.
      if ($this->config->get('conductor.settings')->get('conductor.logging')) {
        $response = [
          'account_id' => $account_id,
          'request_id' => $request_id,
          'request URL' => $url,
          'override_api_key' => $override_api_key,
          'override_shared_secret' => $override_shared_secret,
          'recommendations' => $data,
        ];
        if (isset($data['progress']['error']) && $data['progress']['error'] > 0) {
          $this->logger->error('<pre>Fetch Recommendations API: ' . print_r($response, TRUE) . '</pre>');
        }
        else {
          $this->logger->debug('<pre>Fetch Recommendations API: ' . print_r($response, TRUE) . '</pre>');
        }
      }
    }
    else {
      $this->logger->error('Fetch Recommendations API: Request ID is empty, fetch failed.');
    }

    return $data;
  }

  /**
   * Fetch Request ID.
   *
   * @param int $account_id
   *   Conductor Account ID.
   * @param int $web_property_id
   *   Conductor Web Property ID.
   * @param string $topic
   *   Topic of request.
   * @param int $rank_source_id
   *   Conductor Rank Source ID.
   * @param string $url
   *   Page URL.
   * @param string $body
   *   Body copy of page.
   * @param string $override_api_key
   *   Override for config API Key.
   * @param string $override_shared_secret
   *   Override for config Shared Secret.
   *
   * @throws \Exception
   */
  public function fetchRequestId($account_id, $web_property_id, $topic, $rank_source_id = 1, $url = '', $body = '', $override_api_key = '', $override_shared_secret = '') {
    $page = [];
    // Page param check/construction.
    if (!empty($url)) {
      $page['url'] = $url;
    }
    if (!empty($body)) {
      $page['bodyCopy'] = $body;
    }
    // Construct params.
    $params = [
      'body' => json_encode([
        'input' => [
          'page' => $page,
          'topic' => [
            'phrase' => $topic,
            'rankSourceId' => $rank_source_id,
            'webPropertyId' => $web_property_id,
            'webPropertyName' => '',
          ],
        ],
        'request' => [
          'insights' => [
            "CONTENT_SCORE_SNIPPET",
            "PRIMARY_KEYWORDS_INSIGHT",
            "SECONDARY_KEYWORDS_INSIGHT",
            "HTML_TITLE_COMMON",
            "HTML_META_DESCRIPTION_COMMON",
            "HTML_HEADING1_COMMON",
            "HTML_HEADING2_COMMON",
            "HTML_HEADING3_COMMON",
            "CONTENT_ANALYSIS_INSIGHT",
            "CONTENT_LENGTH_INSIGHT",
            "READABILITY_INSIGHT",
            "QUESTIONS_INSIGHT_V2",
            "CANNIBALIZATION_INSIGHT",
            "HTML_IMAGE_ALT_COMMON",
            "SCHEMA_MARKUP_INSIGHT",
          ],
        ],
      ]),
    ];

    $request_id = $this->conductorConnector->request('POST', '/' . $account_id . '/content-guidance', $params, $override_api_key, $override_shared_secret);
    $request_id = !empty($request_id['request_id']) ? $request_id['request_id'] : '';

    // Log request.
    if ($this->config->get('conductor.settings')->get('conductor.logging')) {
      $request = [
        'account_id' => $account_id,
        'request_id' => $request_id,
        'request_parameters' => $params,
      ];
      $this->logger->debug('<pre>Fetch RequestID API: ' . print_r($request, TRUE) . '</pre>');
    }

    return $request_id;
  }

  /**
   * Get Conductor Account ID.
   */
  public function getAccountId() {
    $account = $this->conductorConnector->request('GET', '/accounts');
    $account_id = '';
    if (is_array($account)) {
      $account_id = $account[0]['accountId'] ?? '';
    }

    return $account_id;
  }

  /**
   * Get list of Rank Sources.
   */
  public function getRankSources(): array {
    try {
      $rank_sources = [];
      $account_id = $this->getAccountId() ?? '';
      if (empty($account_id)) {
        return $rank_sources;
      }

      $rank_source_data = $this->conductorConnector->request('GET', '/' . $account_id . '/rank-sources/content-guidance') ?? [];
      foreach ($rank_source_data as $source) {
        if (empty($source)) {
          continue;
        }
        $rank_sources[$source['id']] = $source['description'];
      }
      asort($rank_sources);

      return $rank_sources;

    }
    catch (ConductorApiRequestException $e) {
      $this->logger->error('Could not generate rank sources due to API connection error.');
      return [];
    }
    catch (\throwable $e) {
      $this->logger->error('Could not generate rank sources : ' . $e->getMessage());
      return [];
    }
  }

  /**
   * Get Request ID.
   */
  public function getRequestId($account_id, $web_property_id, $config, $body_copy, $form_state) {
    // Request new request_id per generation.
    return $this->fetchRequestId(
      $account_id,
      $web_property_id,
      $form_state->getValue(['conductor', 'keyword_container', 'topic']),
      $config->get('conductor.rank_sources'),
      '',
      $body_copy,
    );

  }

  /**
   * Get Conductor Web Property ID.
   *
   * @param int $account_id
   *   Conductor Account ID.
   *
   * @throws \Exception
   */
  public function getWebPropertyId($account_id) {
    $web_property = $this->conductorConnector->request('GET', '/accounts/' . $account_id . '/web-properties');
    $web_property_id = '';
    if (!empty($web_property)) {
      $web_property_id = $web_property[0]['webPropertyId'] ?? '';
    }

    return $web_property_id;
  }

}
