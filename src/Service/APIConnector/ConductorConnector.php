<?php

declare(strict_types=1);

namespace Drupal\conductor\Service\APIConnector;

use Drupal\conductor\Exception\ConductorApiRequestException;
use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;

/**
 * Provides Conductor API Connector.
 *
 * @package Drupal\conductor\Service\ConductorConnector
 */
class ConductorConnector {

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configSettings;

  /**
   * Guzzle HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs class variables.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_service
   *   Config Factory.
   * @param \GuzzleHttp\Client $http_client
   *   Guzzle HTTP Client.
   * @param \Psr\Log\LoggerInterface $logger
   *   Drupal Logger Factory.
   */
  public function __construct(ConfigFactoryInterface $config_service, Client $http_client, LoggerInterface $logger) {
    $this->configSettings = $config_service;
    $this->httpClient = $http_client;
    $this->logger = $logger;
  }

  /**
   * Requests method to Conductor API.
   *
   * @param string $method
   *   Method of request.
   * @param string $endpoint
   *   URL endpoint of request.
   * @param array $options
   *   Array of options for request body.
   * @param string $override_api_key
   *   Override for config API Key.
   * @param string $override_shared_secret
   *   Override for config Shared Secret.
   *
   * @throws \Exception
   */
  public function request($method, $endpoint, $options = [], $override_api_key = '', $override_shared_secret = '') {
    // Authentication credentials.
    $auth_creds = $this->configSettings->get('conductor.settings')->get('conductor') ?? [];
    $api_key = !empty($override_api_key) ? $override_api_key : $auth_creds['api_key'];
    $shared_secret = !empty($override_shared_secret) ? $override_shared_secret : $auth_creds['shared_secret'];
    $base_url = 'https://api.conductor.com/v3';

    // Construct signature.
    $signature = $this->constructSignature($api_key, $shared_secret);

    // Endpoint.
    $url_components = parse_url($endpoint);
    if (empty($url_components['query'])) {
      $uri = $base_url . $endpoint . '?apiKey=' . $api_key . '&sig=' . $signature;
    }
    else {
      $uri = $base_url . $endpoint . '&apiKey=' . $api_key . '&sig=' . $signature;
    }

    // Build Endpoint parameters.
    $headers = [
      'headers' => [
        'Accept' => 'application/json',
        'Content-type' => 'application/json',
      ],
    ];
    $headers = array_merge($headers, $options);

    // Make request.
    $data = [];
    try {
      $client = $this->httpClient->request($method, $uri, $headers);
      // Get data.
      if ($client->getStatusCode() === 200) {
        $data = $client->getBody()->getContents();
        $data = json_decode($data, TRUE);
      }
    }
    catch (\Throwable $e) {
      $this->logger->error($e->getMessage());
      throw new ConductorApiRequestException();
    }

    return $data ?? [];
  }

  /**
   * Construct signature for API requests to Conductor.
   *
   * @param string $api_key
   *   Conductor API Key.
   * @param string $shared_secret
   *   Conductor Shared Secret.
   */
  public function constructSignature($api_key, $shared_secret) {
    return md5($api_key . $shared_secret . time());
  }

}
