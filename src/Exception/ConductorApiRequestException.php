<?php

namespace Drupal\conductor\Exception;

/**
 * Conductor Request Exception.
 */
class ConductorApiRequestException extends \Exception {
}
