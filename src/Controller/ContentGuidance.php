<?php

namespace Drupal\conductor\Controller;

use Drupal\conductor\Service\APIConnector\ConductorAPIService;
use Drupal\conductor\Service\APIConnector\ConductorConnector;
use Drupal\conductor\Service\ConductorHelper;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Content Guidance Endpoint.
 *
 * @codeCoverageIgnore
 */
class ContentGuidance extends ControllerBase {

  /**
   * Conductor Connector.
   *
   * @var \Drupal\conductor\Service\APIConnector\ConductorConnector
   */
  protected $conductorConnector;

  /**
   * Conductor Helper.
   *
   * @var \Drupal\conductor\Service\ConductorHelper
   */
  protected $conductorHelper;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Conductor API Service.
   *
   * @var \Drupal\conductor\Service\APIConnector\ConductorAPIService
   */
  private ConductorAPIService $conductorAPIService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('conductor.connector'),
        $container->get('conductor.helper'),
        $container->get('conductor.api_service'),
        $container->get('request_stack')
      );
  }

  /**
   * Constructs a new object.
   *
   * @param \Drupal\conductor\Service\APIConnector\ConductorConnector $conductor_connector
   *   Conductor Connector service.
   * @param \Drupal\conductor\Service\ConductorHelper $conductor_helper
   *   Conductor Helper service.
   * @param \Drupal\conductor\Service\APIConnector\ConductorAPIService $conductorAPIService
   *   Conductor API service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack object.
   */
  public function __construct(ConductorConnector $conductor_connector, ConductorHelper $conductor_helper, ConductorAPIService $conductorAPIService, RequestStack $request_stack) {
    $this->conductorConnector = $conductor_connector;
    $this->conductorHelper = $conductor_helper;
    $this->conductorAPIService = $conductorAPIService;
    $this->request = $request_stack;
  }

  /**
   * Generate Content Guidance.
   */
  public function generateContentGuidance() {
    // Parameters.
    $api_key = $this->request->getCurrentRequest()->query->get('api_key') ?? '';
    $shared_secret = $this->request->getCurrentRequest()->query->get('shared_secret') ?? '';
    $url = $this->request->getCurrentRequest()->query->get('url') ?? '';
    $body_copy = $this->request->getCurrentRequest()->query->get('body_copy') ?? '';
    $topic = $this->request->getCurrentRequest()->query->get('topic') ?? '';
    $rank_source_id = $this->request->getCurrentRequest()->query->get('rank_source_id') ?? 1;
    // GET Account ID.
    $account = $this->conductorConnector->request('GET', '/accounts', [], $api_key, $shared_secret);
    $account_id = !empty($account[0]['accountId']) ? $account[0]['accountId'] : '';
    // GET Web Property ID.
    $web_property = $this->conductorConnector->request('GET', '/accounts/' . $account_id . '/web-properties', [], $api_key, $shared_secret);
    $web_property_id = !empty($web_property[0]['webPropertyId']) ? $web_property[0]['webPropertyId'] : '';
    // Construct content guidance recommendations.
    $request_id = $this->conductorAPIService->fetchRequestId($account_id, $web_property_id, $topic, $rank_source_id, $url, $body_copy, $api_key, $shared_secret);
    $output = $this->conductorAPIService->fetchRecommendations($account_id, $request_id, $api_key, $shared_secret);

    return new JsonResponse($output);
  }

}
